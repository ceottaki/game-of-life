﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaintCellData.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// Represents data about how to paint a cell.
    /// </summary>
    internal class PaintCellData
    {
        /// <summary>
        /// Gets or sets a value indicating whether the cell should be painted.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the cell should be painted; otherwise, <c>false</c>.
        /// </value>
        public bool IsPainting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the cell should be erased.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the cell should be erased; otherwise, <c>false</c>.
        /// </value>
        public bool IsErasing { get; set; }

        /// <summary>
        /// Gets or sets the position of the cell in the canvas.
        /// </summary>
        /// <value>
        /// The position of the cell in the canvas.
        /// </value>
        public Point Position { get; set; }
    }
}
