﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    using GoL.Logic;

    using Microsoft.Win32;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.ViewModel = this.DataContext as IMainWindowViewModel;

            if (this.ViewModel == null)
            {
                return;
            }

            this.ViewModel.Messenger.Send = this.Send;
            this.ViewModel.ShowOpenFileDialog = this.ShowOpenFileDialog;
            this.ViewModel.ShowSaveFileDialog = this.ShowSaveFileDialog;
        }

        /// <summary>
        /// Gets or sets the view model for this window.
        /// </summary>
        /// <value>
        /// The view model for this window.
        /// </value>
        private IMainWindowViewModel ViewModel { get; set; }

        /// <summary>
        /// Shows the open file dialog.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="dialogTitle">The dialog title.</param>
        /// <returns>The file name selected to be opened.</returns>
        private string ShowOpenFileDialog(string defaultExtension, string filter, string dialogTitle)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
                                                {
                                                    AddExtension = true,
                                                    CheckFileExists = true,
                                                    CheckPathExists = true,
                                                    Multiselect = false,
                                                    FileName = string.Empty,
                                                    DefaultExt = defaultExtension,
                                                    Filter = filter,
                                                    Title = dialogTitle
                                                };

            return openFileDialog.ShowDialog(this).Value ? openFileDialog.FileName : null;
        }

        /// <summary>
        /// Shows the save file dialog.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="dialogTitle">The dialog title.</param>
        /// <returns>The file name selected to be opened.</returns>
        private string ShowSaveFileDialog(string defaultExtension, string filter, string dialogTitle)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                AddExtension = true,
                OverwritePrompt = true,
                CheckPathExists = true,
                FileName = string.Empty,
                DefaultExt = defaultExtension,
                Filter = filter,
                Title = dialogTitle
            };

            return saveFileDialog.ShowDialog(this).Value ? saveFileDialog.FileName : null;
        }

        /// <summary>
        /// Sends the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="messageButton">The buttons to be displayed for the message.</param>
        /// <param name="messageType">The type of the message.</param>
        /// <returns>The option selected by the user after seeing the message.</returns>
        private MessageResult Send(string message, string caption, MessageButton messageButton, MessageType messageType)
        {
            return (MessageResult)MessageBox.Show(this, message, caption, (MessageBoxButton)messageButton, (MessageBoxImage)messageType);
        }
    }
}
