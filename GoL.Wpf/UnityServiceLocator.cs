﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityServiceLocator.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;

    using GoL.DependencyInjection;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// Represents a unity service locator.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated in XAML code.")]
    internal class UnityServiceLocator
    {
        private static IUnityContainer _container;

        /// <summary>
        /// Initialises a new instance of the <see cref="UnityServiceLocator"/> class.
        /// </summary>
        public UnityServiceLocator()
        {
            if (null == _container)
            {
                InitialiseApp();
            }
        }

        /// <summary>
        /// Gets the view model for the main window.
        /// </summary>
        /// <value>
        /// The view model for the main window.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Property is used in XAML code.")]
        public static IMainWindowViewModel MainWindowViewModel
        {
            get
            {
                return _container.Resolve<IMainWindowViewModel>();
            }
        }

        /// <summary>
        /// Initialises the application.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method is called by class constructor which is called in XAML code.")]
        private static void InitialiseApp()
        {
            _container = new UnityContainer();
            InjectionHandler.InitialiseDependencies(_container);
            _container.RegisterType<IMainWindowViewModel, MainWindowViewModel>();
        }
    }
}
