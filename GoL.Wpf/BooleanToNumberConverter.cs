﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BooleanToNumberConverter.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Represents a Boolean to number converter.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated in XAML code.")]
    internal class BooleanToNumberConverter : IValueConverter
    {
        /// <summary>
        /// Converts a the given number in the value to a Boolean returning <c>true</c> if they are both the same or <c>false</c> if they are different.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// <c>true</c> or <c>false</c>.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int numberParameter = 0;
            string parameterAsString = parameter as string;
            if (parameterAsString != null)
            {
                if (!int.TryParse(parameterAsString, out numberParameter))
                {
                    numberParameter = -1;
                }
            }
            else if (parameter is int)
            {
                numberParameter = (int)parameter;
            }

            if (value is int)
            {
                return (int)value == numberParameter;
            }

            return false;
        }

        /// <summary>
        /// Converts a Boolean or Nullable&lt;bool&gt; to the given number passed in the parameter if the original value is <c>true</c> or to -1 if <c>false</c>.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// The <see cref="int"/> given in <paramref name="parameter"/> if <paramref name="value"/> is <c>true</c>; -1 otherwise.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int numberParameter = 0;
            string parameterAsString = parameter as string;
            if (parameterAsString != null)
            {
                if (!int.TryParse(parameterAsString, out numberParameter))
                {
                    numberParameter = -1;
                }
            }
            else if (parameter is int)
            {
                numberParameter = (int)parameter;
            }

            bool result = false;
            if (value is bool)
            {
                result = (bool)value;
            }
            else if (value is bool?)
            {
                bool? tmp = (bool?)value;
                result = tmp.HasValue && tmp.Value;
            }

            return result ? numberParameter : -1;
        }
    }
}
