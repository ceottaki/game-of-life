﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindowViewModel.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Timers;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    using GalaSoft.MvvmLight.Command;

    using GoL.Logic;
    using GoL.Wpf.Annotations;
    using GoL.Wpf.Properties;

    using Timer = System.Timers.Timer;

    /// <summary>
    /// Represents a view model for the main window.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated by dependency injection."), UsedImplicitly]
    internal class MainWindowViewModel : IMainWindowViewModel, IDisposable
    {
        private const int _MinCellDrawingSize = 2;

        private const int _MaxCellDrawingSize = 10;

        private const decimal _DefaultSecondsWaitBetweenIterations = 0.1m;

        private readonly Timer _timer = new Timer((int)(_DefaultSecondsWaitBetweenIterations * 1000));

        private readonly IGameOfLifeProcessor _gameOfLifeProcessor;

        private readonly ObservableCollection<Rectangle> _cells = new ObservableCollection<Rectangle>();

        private readonly Dispatcher _dispatcher;

        private int _currentCellSize = _MaxCellDrawingSize;

        private Rect _gridRectangle = new Rect(0, 0, _MaxCellDrawingSize, _MaxCellDrawingSize);

        private Board _board = new Board();

        private Board _initialBoard = new Board();

        private volatile bool _isRunning;

        private volatile object _lockObject = new object();

        private decimal _secondsWaitBetweenIterations;

        private string _startStopIterationsCommandDisplayText;

        private int _canvasWidth;

        private int _canvasHeight;

        private int _currentIteration;

        private bool _displayControls = true;

        private bool _autoIncreaseResolution;

        private bool _displayGridLines;

        private string _currentFileName;

        private bool _boardHasChanged;

        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowViewModel" /> class.
        /// </summary>
        /// <param name="gameOfLifeProcessor">The game of life processor.</param>
        /// <param name="messenger">The messenger.</param>
        /// <exception cref="System.ArgumentNullException">gameOfLifeProcessor; Value of gameOfLifeProcessor cannot be null.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges", Justification = "Firing the timer more than one time per second is necessary for correct user experience.")]
        public MainWindowViewModel(IGameOfLifeProcessor gameOfLifeProcessor, IMessenger messenger)
        {
            if (null == gameOfLifeProcessor)
            {
                throw new ArgumentNullException("gameOfLifeProcessor", string.Format(CultureInfo.CurrentCulture, Resources.ValueCannotBeNull, "gameOfLifeProcessor"));
            }

            if (null == messenger)
            {
                throw new ArgumentNullException("messenger", string.Format(CultureInfo.CurrentCulture, Resources.ValueCannotBeNull, "messenger"));
            }

            _gameOfLifeProcessor = gameOfLifeProcessor;
            _dispatcher = Dispatcher.CurrentDispatcher;

            this.ResizeBoard();
            _timer.Elapsed += TimerOnElapsed;

            this.Messenger = messenger;
            this.SecondsWaitBetweenIterations = _DefaultSecondsWaitBetweenIterations;
            this.StartStopIterationsCommandDisplayText = "Start";
            this.StartStopIterationsCommand = new RelayCommand(this.ExecuteStartStopIterationsCommand);
            this.NextIterationCommand = new RelayCommand(this.ExecuteNextIterationCommand, this.CanExecuteNextIterationCommand);
            this.ClearBoardCommand = new RelayCommand(this.ExecuteClearBoardCommand, this.CanExecuteClearBoardCommand);
            this.GoBackToStartCommand = new RelayCommand(this.ExecuteGoBackToStartCommand, this.CanExecuteGoBackToStartCommand);
            this.PaintCellsCommand = new RelayCommand<PaintCellData>(this.ExecutePaintCellsCommand, this.CanExecutePaintCellsCommand);
            this.OpenFileCommand = new RelayCommand(this.ExecuteOpenFileCommand, this.CanExecuteOpenFileCommand);
            this.CloseFileCommand = new RelayCommand(this.ExecuteCloseFileCommand, this.CanExecuteCloseFileCommand);
            this.SaveFileCommand = new RelayCommand<bool>(this.ExecuteSaveFileCommand, this.CanExecuteSaveFileCommand);
            this.ExitApplicationCommand = new RelayCommand(this.ExecuteExitApplicationCommand);
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the width of the canvas.
        /// </summary>
        /// <value>
        /// The width of the canvas.
        /// </value>
        public int CanvasWidth
        {
            get
            {
                return _canvasWidth;
            }

            set
            {
                if (value == _canvasWidth)
                {
                    return;
                }

                _canvasWidth = value;
                this.OnPropertyChanged();
                this.ResizeBoard();
            }
        }

        /// <summary>
        /// Gets or sets the height of the canvas.
        /// </summary>
        /// <value>
        /// The height of the canvas.
        /// </value>
        public int CanvasHeight
        {
            get
            {
                return _canvasHeight;
            }

            set
            {
                if (value == _canvasHeight)
                {
                    return;
                }

                _canvasHeight = value;
                this.OnPropertyChanged();
                this.ResizeBoard();
            }
        }

        /// <summary>
        /// Gets the cells in the board.
        /// </summary>
        /// <value>
        /// The cells in the board.
        /// </value>
        public IEnumerable<Rectangle> Cells
        {
            get
            {
                return _cells;
            }
        }

        /// <summary>
        /// Gets the minimum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The minimum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </value>
        public decimal MinSecondsWaitBetweenIterations
        {
            get
            {
                return 0.1m;
            }
        }

        /// <summary>
        /// Gets the maximum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The maximum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </value>
        public decimal MaxSecondsWaitBetweenIterations
        {
            get
            {
                return 5m;
            }
        }

        /// <summary>
        /// Gets or sets the number of seconds to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The number of seconds to wait between iterations when running iterations automatically.
        /// </value>
        public decimal SecondsWaitBetweenIterations
        {
            get
            {
                return _secondsWaitBetweenIterations;
            }

            set
            {
                if ((value == _secondsWaitBetweenIterations) || (value > this.MaxSecondsWaitBetweenIterations) || (value < this.MinSecondsWaitBetweenIterations))
                {
                    return;
                }

                _secondsWaitBetweenIterations = value;
                this.OnPropertyChanged();
                _timer.Interval = (int)(_secondsWaitBetweenIterations * 1000);
            }
        }

        /// <summary>
        /// Gets the display text for the start/stop iterations command.
        /// </summary>
        /// <value>
        /// The display text for the start/stop iterations command. Either "Start" or "Stop".
        /// </value>
        public string StartStopIterationsCommandDisplayText
        {
            get
            {
                return _startStopIterationsCommandDisplayText;
            }

            private set
            {
                if (value == _startStopIterationsCommandDisplayText)
                {
                    return;
                }

                _startStopIterationsCommandDisplayText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the current iteration.
        /// </summary>
        /// <value>
        /// The current iteration.
        /// </value>
        public int CurrentIteration
        {
            get
            {
                return _currentIteration;
            }

            private set
            {
                if (value == _currentIteration)
                {
                    return;
                }

                _currentIteration = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the name of the currently opened file.
        /// </summary>
        /// <value>
        /// The name of the currently opened file.
        /// </value>
        public string CurrentFileName
        {
            get
            {
                return _currentFileName;
            }

            private set
            {
                if (value == _currentFileName)
                {
                    return;
                }

                _currentFileName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display grid lines in the board.
        /// </summary>
        /// <value>
        /// <c>true</c> if grid lines should be displayed in the board; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayGridLines
        {
            get
            {
                return _displayGridLines;
            }

            set
            {
                if (value.Equals(_displayGridLines))
                {
                    return;
                }

                _displayGridLines = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the controls.
        /// </summary>
        /// <value>
        /// <c>true</c> if the controls should be displayed; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayControls
        {
            get
            {
                return _displayControls;
            }

            set
            {
                if (value.Equals(_displayControls))
                {
                    return;
                }

                _displayControls = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to automatically increase the board's resolution by reducing the size of the cell when needed.
        /// </summary>
        /// <value>
        /// <c>true</c> if the board's resolution should be automatically increased by reducing the size of the cell when needed; otherwise, <c>false</c>.
        /// </value>
        public bool AutoIncreaseResolution
        {
            get
            {
                return _autoIncreaseResolution;
            }

            set
            {
                if (value.Equals(_autoIncreaseResolution))
                {
                    return;
                }

                _autoIncreaseResolution = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current cell size.
        /// </summary>
        /// <value>
        /// The current cell size.
        /// </value>
        public int CurrentCellSize
        {
            get
            {
                return _currentCellSize;
            }

            set
            {
                if ((value == _currentCellSize) || (value < _MinCellDrawingSize) || (value > _MaxCellDrawingSize))
                {
                    return;
                }

                _currentCellSize = value;
                this.OnPropertyChanged();
                this.GridRectangle = new Rect(0, 0, _currentCellSize, _currentCellSize);

                this.ResizeBoard();
            }
        }

        /// <summary>
        /// Gets the grid rectangle.
        /// </summary>
        /// <value>
        /// The grid rectangle.
        /// </value>
        public Rect GridRectangle
        {
            get
            {
                return _gridRectangle;
            }

            private set
            {
                if (value.Equals(_gridRectangle))
                {
                    return;
                }

                _gridRectangle = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the messenger that can be used to send messages to the front-end.
        /// </summary>
        /// <value>
        /// The messenger that can be used to send messages to the front-end.
        /// </value>
        public IMessenger Messenger { get; private set; }

        /// <summary>
        /// Gets or sets the method used to open a file.
        /// </summary>
        /// <value>
        /// The method used to open a file.
        /// </value>
        /// <remarks>
        /// Parameters of the method are in this order: default extension, filter, dialog title. The return value is the file name.
        /// </remarks>
        public Func<string, string, string, string> ShowOpenFileDialog { get; set; }

        /// <summary>
        /// Gets or sets the method used to save a file.
        /// </summary>
        /// <value>
        /// The method used to save a file.
        /// </value>
        /// <remarks>
        /// Parameters of the method are in this order: default extension, filter, dialog title. The return value is the file name.
        /// </remarks>
        public Func<string, string, string, string> ShowSaveFileDialog { get; set; }

        /// <summary>
        /// Gets the command to start/stop iterations.
        /// </summary>
        /// <value>
        /// The command to start/stop iterations.
        /// </value>
        public ICommand StartStopIterationsCommand { get; private set; }

        /// <summary>
        /// Gets the command to manually move to the next iteration.
        /// </summary>
        /// <value>
        /// The command to manually move to the next iteration.
        /// </value>
        public ICommand NextIterationCommand { get; private set; }

        /// <summary>
        /// Gets the command to clear the board.
        /// </summary>
        /// <value>
        /// The command to clear the board.
        /// </value>
        public ICommand ClearBoardCommand { get; private set; }

        /// <summary>
        /// Gets the command to go back to the initial design before starting the iterations.
        /// </summary>
        /// <value>
        /// The command to go back to the initial design before starting the iterations.
        /// </value>
        public ICommand GoBackToStartCommand { get; private set; }

        /// <summary>
        /// Gets the command to paint cells.
        /// </summary>
        /// <value>
        /// The command to paint cells.
        /// </value>
        public ICommand PaintCellsCommand { get; private set; }

        /// <summary>
        /// Gets the command to open a file.
        /// </summary>
        /// <value>
        /// The command to open a file.
        /// </value>
        public ICommand OpenFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to close the currently opened file.
        /// </summary>
        /// <value>
        /// The command to close the currently opened file.
        /// </value>
        public ICommand CloseFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to save a file.
        /// </summary>
        /// <value>
        /// The command to save a file.
        /// </value>
        public ICommand SaveFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to exit the application.
        /// </summary>
        /// <value>
        /// The command to exit the application.
        /// </value>
        public ICommand ExitApplicationCommand { get; private set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _timer.Dispose();
            }
        }

        /// <summary>
        /// Executes the start stop iterations command.
        /// </summary>
        private void ExecuteStartStopIterationsCommand()
        {
            _isRunning = !_isRunning;
            this.StartStopIterationsCommandDisplayText = _isRunning ? "Stop" : "Start";

            if (_isRunning)
            {
                _timer.Start();
            }
            else
            {
                _timer.Stop();
            }
        }

        /// <summary>
        /// Executes the next iteration command.
        /// </summary>
        private void ExecuteNextIterationCommand()
        {
            this.RunIteration();
            _dispatcher.BeginInvoke(DispatcherPriority.Input, (Action)this.UpdateCellsFromBoard);
        }

        /// <summary>
        /// Determines whether the next iteration command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteNextIterationCommand()
        {
            return !_isRunning;
        }

        /// <summary>
        /// Executes the clear board command.
        /// </summary>
        private void ExecuteClearBoardCommand()
        {
            bool[,] currentPattern = _board.GetPattern();
            _board = new Board();
            _board.SetPattern(_gameOfLifeProcessor.CreateNewPattern(currentPattern.GetLength(0), currentPattern.GetLength(1)));
            _initialBoard = new Board();
            _initialBoard.SetPattern(_gameOfLifeProcessor.CreateNewPattern(currentPattern.GetLength(0), currentPattern.GetLength(1)));
            _boardHasChanged = true;
            this.CurrentIteration = 0;
            this.UpdateCellsFromBoard();
        }

        /// <summary>
        /// Determines whether the clear board command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteClearBoardCommand()
        {
            return !_isRunning;
        }

        /// <summary>
        /// Executes the go back to start command.
        /// </summary>
        private void ExecuteGoBackToStartCommand()
        {
            this.CurrentIteration = 0;
            _board.SetPattern(_initialBoard.GetPattern());
            this.ResizeBoard();
        }

        /// <summary>
        /// Determines whether the go back to start command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteGoBackToStartCommand()
        {
            return (!_isRunning) && (_initialBoard != null);
        }

        /// <summary>
        /// Executes the paint cells command.
        /// </summary>
        /// <param name="paintCellData">The paint cell data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private void ExecutePaintCellsCommand(PaintCellData paintCellData)
        {
            bool[,] pattern = _board.GetPattern();
            int x = (int)(paintCellData.Position.X / this.CurrentCellSize);
            int y = (int)(paintCellData.Position.Y / this.CurrentCellSize);
            if ((x <= pattern.GetLength(0)) && (y <= pattern.GetLength(1)) && (x >= 0) && (y >= 0))
            {
                pattern[x, y] = paintCellData.IsPainting;
                _board.SetPattern(pattern);
                _boardHasChanged = true;
            }

            bool[,] initialBoardPattern = new bool[pattern.GetLength(0), pattern.GetLength(1)];
            Array.Copy(pattern, initialBoardPattern, pattern.Length);
            _initialBoard.SetPattern(initialBoardPattern);

            this.CurrentIteration = 0;
            this.UpdateCellsFromBoard();
        }

        /// <summary>
        /// Determines whether the paint cells command can be executed with the given paint cell data.
        /// </summary>
        /// <param name="paintCellData">The paint cell data.</param>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecutePaintCellsCommand(PaintCellData paintCellData)
        {
            return !_isRunning && (paintCellData != null) && (paintCellData.IsErasing || paintCellData.IsPainting);
        }

        /// <summary>
        /// Executes the open file command.
        /// </summary>
        private void ExecuteOpenFileCommand()
        {
            MessageResult answer = MessageResult.No;
            if (_boardHasChanged)
            {
                answer = this.SendMessage("Do you want to save the changes you made to this file?", "Save Changes?", MessageButton.YesNoCancel, MessageType.Question);
            }

            if (MessageResult.Yes == answer)
            {
                this.ExecuteSaveFileCommand(false);
            }

            if (MessageResult.Cancel == answer)
            {
                return;
            }

            string fileName = this.DisplayOpenFileDialog(
                ".rle",
                "Run Length Encoded (*.rle)|*.rle|Life 1.05 / 1.06 (*.lif, *.life)|*.lif;*.life|Plaintext (*.cells)|*.cells|All Supported Formats|*.rle;*.lif;*.life;*.cells|All Files (*.*)|*.*",
                "Open File");

            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            GameOfLifeFileType fileType;
            string[] fileNameSplit = fileName.Split('.');
            if (fileNameSplit.Length == 0)
            {
                fileType = GameOfLifeFileType.Rle;
            }
            else
            {
                switch (fileNameSplit[fileNameSplit.Length - 1].ToUpperInvariant())
                {
                    case "LIF":
                    case "LIFE":
                        fileType = GameOfLifeFileType.Life105Or106;
                        break;

                    case "CELLS":
                        fileType = GameOfLifeFileType.Plaintext;
                        break;

                    default:
                        fileType = GameOfLifeFileType.Rle;
                        break;
                }
            }

            Board loadedBoard = null;
            try
            {
                loadedBoard = _gameOfLifeProcessor.LoadBoard(fileName, fileType, this.CanvasWidth / this.CurrentCellSize, this.CanvasHeight / this.CurrentCellSize);
            }
            catch (Exception exception)
            {
                this.SendMessage(
                    string.Format(CultureInfo.CurrentCulture, "There was an error while loading the board:{0}{1}", Environment.NewLine, exception.Message),
                    "Error Loading Board",
                    MessageButton.Ok,
                    MessageType.Error);
            }

            if (loadedBoard == null)
            {
                return;
            }

            this.CurrentFileName = fileName;

            bool[,] loadedPattern = loadedBoard.GetPattern();
            if (_autoIncreaseResolution)
            {
                while ((this.CurrentCellSize > _MinCellDrawingSize)
                       && ((loadedPattern.GetLength(0) > this.CanvasWidth / this.CurrentCellSize) || (loadedPattern.GetLength(1) > this.CanvasHeight / this.CurrentCellSize)))
                {
                    this.CurrentCellSize--;
                }
            }

            if (((!_autoIncreaseResolution) || (this.CurrentCellSize == _MinCellDrawingSize))
                && ((loadedPattern.GetLength(0) > this.CanvasWidth / this.CurrentCellSize) || (loadedPattern.GetLength(1) > this.CanvasHeight / this.CurrentCellSize)))
            {
                this.SendMessage(
                    "The loaded board is bigger than what can be displayed on screen and will be cropped.",
                    "Board Will Be Cropped",
                    MessageButton.Ok,
                    MessageType.Warning);
            }

            _board.SetPattern(loadedPattern);
            _initialBoard.SetPattern(loadedPattern);
            this.CurrentIteration = 0;
            _boardHasChanged = false;
            this.ResizeBoard();
        }

        /// <summary>
        /// Determines whether the open file command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteOpenFileCommand()
        {
            return !_isRunning;
        }

        /// <summary>
        /// Executes the close file command.
        /// </summary>
        private void ExecuteCloseFileCommand()
        {
            MessageResult answer = MessageResult.No;
            if (_boardHasChanged)
            {
                answer = this.SendMessage("Do you want to save the changes you made to this file?", "Save Changes?", MessageButton.YesNoCancel, MessageType.Question);
            }

            if (MessageResult.Yes == answer)
            {
                this.ExecuteSaveFileCommand(false);
            }

            if (MessageResult.Cancel == answer)
            {
                return;
            }

            this.CurrentFileName = null;
            this.ExecuteClearBoardCommand();
            _boardHasChanged = false;
        }

        /// <summary>
        /// Determines whether the close file command can be executed.
        /// </summary>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteCloseFileCommand()
        {
            return (!_isRunning) && (!string.IsNullOrEmpty(this.CurrentFileName));
        }

        /// <summary>
        /// Executes the save file command.
        /// </summary>
        /// <param name="promptFileName">if set to <c>true</c>, prompt the user for a file name.</param>
        private void ExecuteSaveFileCommand(bool promptFileName)
        {
            if (promptFileName)
            {
                string fileName = this.DisplaySaveFileDialog(
                    ".rle",
                    "Run Length Encoded (*.rle)|*.rle|Life 1.05 (*.lif)|*.lif|Life 1.06 (*.life)|*.life|Plaintext (*.cells)|*.cells|All Supported Formats|*.rle;*.lif;*.life;*.cells|All Files (*.*)|*.*",
                    "Open File");

                if (string.IsNullOrEmpty(fileName))
                {
                    this.SendMessage("No file name was given when trying to save a file.\r\nThe file has not been saved.", "No File Name", MessageButton.Ok, MessageType.Warning);
                    return;
                }

                this.CurrentFileName = fileName;
            }

            GameOfLifeFileType fileType;
            string[] fileNameSplit = this.CurrentFileName.Split('.');
            if (fileNameSplit.Length == 0)
            {
                fileType = GameOfLifeFileType.Rle;
            }
            else
            {
                switch (fileNameSplit[fileNameSplit.Length - 1].ToUpperInvariant())
                {
                    case "LIF":
                        fileType = GameOfLifeFileType.Life105;
                        break;

                    case "LIFE":
                        fileType = GameOfLifeFileType.Life106;
                        break;

                    case "CELLS":
                        fileType = GameOfLifeFileType.Plaintext;
                        break;

                    default:
                        fileType = GameOfLifeFileType.Rle;
                        break;
                }
            }

            try
            {
                _gameOfLifeProcessor.SaveBoard(_board, this.CurrentFileName, fileType, true);
                _boardHasChanged = false;
            }
            catch (Exception exception)
            {
                this.SendMessage(
                    string.Format(CultureInfo.CurrentCulture, "There was an error while saving the board:{0}{1}", Environment.NewLine, exception.Message),
                    "Error Saving Board",
                    MessageButton.Ok,
                    MessageType.Error);
            }
        }

        /// <summary>
        /// Determines whether the save file command can be executed.
        /// </summary>
        /// <param name="promptFileName">if set to <c>true</c>, prompt the user for a file name.</param>
        /// <returns><c>true</c> if the command can be executed; <c>false</c> otherwise.</returns>
        private bool CanExecuteSaveFileCommand(bool promptFileName)
        {
            return (!_isRunning) && (promptFileName || (!string.IsNullOrEmpty(this.CurrentFileName)));
        }

        /// <summary>
        /// Executes the exit application command.
        /// </summary>
        private void ExecuteExitApplicationCommand()
        {
            MessageResult answer = MessageResult.No;
            if (_boardHasChanged)
            {
                answer = this.SendMessage("Do you want to save the changes you made to this file?", "Save Changes?", MessageButton.YesNoCancel, MessageType.Question);
            }

            if (MessageResult.Yes == answer)
            {
                this.ExecuteSaveFileCommand(string.IsNullOrEmpty(this.CurrentFileName));
            }

            if (MessageResult.Cancel == answer)
            {
                return;
            }

            Application.Current.Shutdown();
        }

        /// <summary>
        /// Updates the cells from the board.
        /// </summary>
        private void UpdateCellsFromBoard()
        {
            _cells.Clear();
            bool[,] pattern = _board.GetPattern();
            for (int x = 0; x < pattern.GetLength(0); x++)
            {
                for (int y = 0; y < pattern.GetLength(1); y++)
                {
                    if (pattern[x, y])
                    {
                        Rectangle cell = new Rectangle { Width = this.CurrentCellSize, Height = this.CurrentCellSize, Fill = Brushes.Black };
                        Canvas.SetLeft(cell, x * this.CurrentCellSize);
                        Canvas.SetTop(cell, y * this.CurrentCellSize);
                        _cells.Add(cell);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Elapsed event of the timer.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="elapsedEventArgs">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            this.ExecuteNextIterationCommand();
        }

        /// <summary>
        /// Runs an iteration.
        /// </summary>
        private void RunIteration()
        {
            _board = _gameOfLifeProcessor.ProcessIteration(_board);
            this.CurrentIteration++;
        }

        /// <summary>
        /// Resizes the board according to the canvas size.
        /// </summary>
        private void ResizeBoard()
        {
            lock (_lockObject)
            {
                bool[,] pattern = _board.GetPattern();
                if (_autoIncreaseResolution)
                {
                    while ((pattern != null) && (this.CurrentCellSize > _MinCellDrawingSize)
                           && ((pattern.GetLength(0) > (this.CanvasWidth / this.CurrentCellSize)) || (pattern.GetLength(1) > (this.CanvasHeight / this.CurrentCellSize))))
                    {
                        this.CurrentCellSize--;
                    }
                }

                bool[,] resizedBoard = _gameOfLifeProcessor.CreateNewPattern(this.CanvasWidth / this.CurrentCellSize, this.CanvasHeight / this.CurrentCellSize);
                if (pattern != null)
                {
                    for (int x = 0; x < Math.Min(pattern.GetLength(0), resizedBoard.GetLength(0)); x++)
                    {
                        for (int y = 0; y < Math.Min(pattern.GetLength(1), resizedBoard.GetLength(1)); y++)
                        {
                            resizedBoard[x, y] = pattern[x, y];
                        }
                    }
                }

                _board.SetPattern(resizedBoard);
                this.UpdateCellsFromBoard();
            }
        }

        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="messageButton">The message button.</param>
        /// <param name="messageType">The type of the message.</param>
        /// <returns>The message result.</returns>
        private MessageResult SendMessage(string message, string caption, MessageButton messageButton, MessageType messageType)
        {
            return this.Messenger.Send != null ? this.Messenger.Send(message, caption, messageButton, messageType) : MessageResult.None;
        }

        /// <summary>
        /// Opens the file dialog.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="dialogTitle">The dialog title.</param>
        /// <returns>The file name that was selected if any.</returns>
        private string DisplayOpenFileDialog(string defaultExtension, string filter, string dialogTitle)
        {
            return this.ShowOpenFileDialog != null ? this.ShowOpenFileDialog(defaultExtension, filter, dialogTitle) : null;
        }

        /// <summary>
        /// Opens the file dialog.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="dialogTitle">The dialog title.</param>
        /// <returns>The file name that was selected if any.</returns>
        private string DisplaySaveFileDialog(string defaultExtension, string filter, string dialogTitle)
        {
            return this.ShowSaveFileDialog != null ? this.ShowSaveFileDialog(defaultExtension, filter, dialogTitle) : null;
        }

        /// <summary>
        /// Called when the value of a property has changed to raise the appropriate event.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}