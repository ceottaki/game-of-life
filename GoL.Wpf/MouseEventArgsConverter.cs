﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MouseEventArgsConverter.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Input;

    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Represents a mouse event arguments converter.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated in XAML code.")]
    internal class MouseEventArgsConverter : IEventArgsConverter
    {
        /// <summary>
        /// The method used to convert the EventArgs instance.
        /// </summary>
        /// <param name="value">An instance of EventArgs passed by the
        ///             event that the EventToCommand instance is handling.</param>
        /// <param name="parameter">An optional parameter used for the conversion. Use
        ///             the 
        /// <see cref="P:GalaSoft.MvvmLight.Command.EventToCommand.EventArgsConverterParameter" /> property
        ///             to set this value. This may be null.</param>
        /// <returns>
        /// The converted value.
        /// </returns>
        public object Convert(object value, object parameter)
        {
            MouseEventArgs eventArgs = (MouseEventArgs)value;
            FrameworkElement layoutRoot = (FrameworkElement)parameter;

            PaintCellData result = new PaintCellData
                                       {
                                           Position = eventArgs.GetPosition(layoutRoot),
                                           IsErasing = MouseButtonState.Pressed == eventArgs.RightButton,
                                           IsPainting = MouseButtonState.Pressed == eventArgs.LeftButton
                                       };
            return result;
        }
    }
}
