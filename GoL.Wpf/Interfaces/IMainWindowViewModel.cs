﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMainWindowViewModel.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Shapes;

    using GoL.Logic;

    /// <summary>
    /// Defines properties and methods for an implementation of a view model for the main window.
    /// </summary>
    internal interface IMainWindowViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the width of the canvas.
        /// </summary>
        /// <value>
        /// The width of the canvas.
        /// </value>
        int CanvasWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the canvas.
        /// </summary>
        /// <value>
        /// The height of the canvas.
        /// </value>
        int CanvasHeight { get; set; }

        /// <summary>
        /// Gets the cells in the board.
        /// </summary>
        /// <value>
        /// The cells in the board.
        /// </value>
        IEnumerable<Rectangle> Cells { get; }

        /// <summary>
        /// Gets the minimum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The minimum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </value>
        decimal MinSecondsWaitBetweenIterations { get; }

        /// <summary>
        /// Gets the maximum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The maximum number of seconds that can be set to wait between iterations when running iterations automatically.
        /// </value>
        decimal MaxSecondsWaitBetweenIterations { get; }

        /// <summary>
        /// Gets or sets the number of seconds to wait between iterations when running iterations automatically.
        /// </summary>
        /// <value>
        /// The number of seconds to wait between iterations when running iterations automatically.
        /// </value>
        decimal SecondsWaitBetweenIterations { get; set; }

        /// <summary>
        /// Gets the display text for the start/stop iterations command.
        /// </summary>
        /// <value>
        /// The display text for the start/stop iterations command. Either "Start" or "Stop".
        /// </value>
        string StartStopIterationsCommandDisplayText { get; }

        /// <summary>
        /// Gets the current iteration.
        /// </summary>
        /// <value>
        /// The current iteration.
        /// </value>
        int CurrentIteration { get; }

        /// <summary>
        /// Gets the name of the currently opened file.
        /// </summary>
        /// <value>
        /// The name of the currently opened file.
        /// </value>
        string CurrentFileName { get; }

        /// <summary>
        /// Gets or sets a value indicating whether to display grid lines in the board.
        /// </summary>
        /// <value>
        ///   <c>true</c> if grid lines should be displayed in the board; otherwise, <c>false</c>.
        /// </value>
        bool DisplayGridLines { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display the controls.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the controls should be displayed; otherwise, <c>false</c>.
        /// </value>
        bool DisplayControls { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to automatically increase the board's resolution by reducing the size of the cell when needed.
        /// </summary>
        /// <value>
        /// <c>true</c> if the board's resolution should be automatically increased by reducing the size of the cell when needed; otherwise, <c>false</c>.
        /// </value>
        bool AutoIncreaseResolution { get; set; }

        /// <summary>
        /// Gets or sets the current cell size.
        /// </summary>
        /// <value>
        /// The current cell size.
        /// </value>
        int CurrentCellSize { get; set; }

        /// <summary>
        /// Gets the grid rectangle.
        /// </summary>
        /// <value>
        /// The grid rectangle.
        /// </value>
        Rect GridRectangle { get; }

        /// <summary>
        /// Gets the messenger that can be used to send messages to the front-end.
        /// </summary>
        /// <value>
        /// The messenger that can be used to send messages to the front-end.
        /// </value>
        IMessenger Messenger { get; }

        /// <summary>
        /// Gets or sets the method used to show an open file dialog.
        /// </summary>
        /// <value>
        /// The method used to show an open file dialog.
        /// </value>
        /// <remarks>
        /// Parameters of the method are in this order: default extension, filter, dialog title. The return value is the file name.
        /// </remarks>
        Func<string, string, string, string> ShowOpenFileDialog { get; set; }

        /// <summary>
        /// Gets or sets the method used to save a file.
        /// </summary>
        /// <value>
        /// The method used to save a file.
        /// </value>
        /// <remarks>
        /// Parameters of the method are in this order: default extension, filter, dialog title. The return value is the file name.
        /// </remarks>
        Func<string, string, string, string> ShowSaveFileDialog { get; set; }
            
        /// <summary>
        /// Gets the command to start/stop iterations.
        /// </summary>
        /// <value>
        /// The command to start/stop iterations.
        /// </value>
        ICommand StartStopIterationsCommand { get; }

        /// <summary>
        /// Gets the command to manually move to the next iteration.
        /// </summary>
        /// <value>
        /// The command to manually move to the next iteration.
        /// </value>
        ICommand NextIterationCommand { get; }

        /// <summary>
        /// Gets the command to clear the board.
        /// </summary>
        /// <value>
        /// The command to clear the board.
        /// </value>
        ICommand ClearBoardCommand { get; }

        /// <summary>
        /// Gets the command to go back to the initial design before starting the iterations.
        /// </summary>
        /// <value>
        /// The command to go back to the initial design before starting the iterations.
        /// </value>
        ICommand GoBackToStartCommand { get; }

        /// <summary>
        /// Gets the command to paint cells.
        /// </summary>
        /// <value>
        /// The command to paint cells.
        /// </value>
        ICommand PaintCellsCommand { get; }

        /// <summary>
        /// Gets the command to open a file.
        /// </summary>
        /// <value>
        /// The command to open a file.
        /// </value>
        ICommand OpenFileCommand { get; }

        /// <summary>
        /// Gets the command to close the currently opened file.
        /// </summary>
        /// <value>
        /// The command to close the currently opened file.
        /// </value>
        ICommand CloseFileCommand { get; }

        /// <summary>
        /// Gets the command to save a file.
        /// </summary>
        /// <value>
        /// The command to save a file.
        /// </value>
        ICommand SaveFileCommand { get; }

        /// <summary>
        /// Gets the command to exit the application.
        /// </summary>
        /// <value>
        /// The command to exit the application.
        /// </value>
        ICommand ExitApplicationCommand { get; }
    }
}
