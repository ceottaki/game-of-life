﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SizeObserver.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// Represents a size observer behaviour that allows the actual size of a control to be bound to with one way to source.
    /// </summary>
    public static class SizeObserver
    {
        /// <summary>
        /// The observe dependency property.
        /// </summary>
        public static readonly DependencyProperty ObserveProperty = DependencyProperty.RegisterAttached(
            "Observe",
            typeof(bool),
            typeof(SizeObserver),
            new FrameworkPropertyMetadata(OnObserveChanged));

        /// <summary>
        /// The observed width dependency property.
        /// </summary>
        public static readonly DependencyProperty ObservedWidthProperty = DependencyProperty.RegisterAttached("ObservedWidth", typeof(double), typeof(SizeObserver));

        /// <summary>
        /// The observed height dependency property.
        /// </summary>
        public static readonly DependencyProperty ObservedHeightProperty = DependencyProperty.RegisterAttached("ObservedHeight", typeof(double), typeof(SizeObserver));

        /// <summary>
        /// Gets the value of the observe property of a given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <returns>The value of the observe property of a given element.</returns>
        public static bool GetObserve(FrameworkElement frameworkElement)
        {
            if (null == frameworkElement)
            {
                return false;
            }

            return (bool)frameworkElement.GetValue(ObserveProperty);
        }

        /// <summary>
        /// Sets the observe property of a given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <param name="observe">The value to set the property with.</param>
        public static void SetObserve(FrameworkElement frameworkElement, bool observe)
        {
            if (null == frameworkElement)
            {
                return;
            }

            frameworkElement.SetValue(ObserveProperty, observe);
        }

        /// <summary>
        /// Gets the value of the width property of the given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <returns>The value of the width property of the given element.</returns>
        public static double GetObservedWidth(FrameworkElement frameworkElement)
        {
            if (null == frameworkElement)
            {
                return 0;
            }

            return (double)frameworkElement.GetValue(ObservedWidthProperty);
        }

        /// <summary>
        /// Sets the width property of the given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <param name="observedWidth">The value to set the property with.</param>
        public static void SetObservedWidth(FrameworkElement frameworkElement, double observedWidth)
        {
            if (null == frameworkElement)
            {
                return;
            }

            frameworkElement.SetValue(ObservedWidthProperty, observedWidth);
        }

        /// <summary>
        /// Gets the value of the height property of the given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <returns>The value of the height property of the given element.</returns>
        public static double GetObservedHeight(FrameworkElement frameworkElement)
        {
            if (null == frameworkElement)
            {
                return 0;
            }

            return (double)frameworkElement.GetValue(ObservedHeightProperty);
        }

        /// <summary>
        /// Sets the height property of the given element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        /// <param name="observedHeight">The value to set the property with.</param>
        public static void SetObservedHeight(FrameworkElement frameworkElement, double observedHeight)
        {
            if (null == frameworkElement)
            {
                return;
            }

            frameworkElement.SetValue(ObservedHeightProperty, observedHeight);
        }

        /// <summary>
        /// Called when the observe property has changed.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnObserveChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement frameworkElement = (FrameworkElement)dependencyObject;

            if ((bool)e.NewValue)
            {
                frameworkElement.SizeChanged += OnFrameworkElementSizeChanged;
                UpdateObservedSizesForFrameworkElement(frameworkElement);
            }
            else
            {
                frameworkElement.SizeChanged -= OnFrameworkElementSizeChanged;
            }
        }

        /// <summary>
        /// Called when the framework element size changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private static void OnFrameworkElementSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateObservedSizesForFrameworkElement((FrameworkElement)sender);
        }

        /// <summary>
        /// Updates the observed sizes for the framework element.
        /// </summary>
        /// <param name="frameworkElement">The framework element.</param>
        private static void UpdateObservedSizesForFrameworkElement(FrameworkElement frameworkElement)
        {
            frameworkElement.SetCurrentValue(ObservedWidthProperty, frameworkElement.ActualWidth);
            frameworkElement.SetCurrentValue(ObservedHeightProperty, frameworkElement.ActualHeight);
        }
    }
}
