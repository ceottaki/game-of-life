# README #


## What's this all about

Welcome to the Game of Life repository! This repository provides an open source implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) in .NET 4.5 using C# and WPF.

While there are many implementations of the Game of Life out there, including a few in .NET and C# and several open source ones, it is a good exercise to be implemented and that was the spirit that started this instance.
This implementation is aimed at providing an architecture that can be considered good at an enterprise level, keeping to both coding and engineering best practices as much as possible.

With that in mind this application can grow to implement several other aspects beyond the basics of Conway's Game of Life as well as other cellular automata while also providing educational value for budding developers
and an experimentation platform for anyone interested in cellular automata.

If you would like to contribute to this project, please read the [wiki](https://bitbucket.org/ceottaki/game-of-life/wiki) for a few guidelines. The application and its code are licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.en.html).


## Current status

These are early days in this project and the current version being worked on is 0.1 and there are no released versions, but although the current application is not fully implemented the basic functionality is present and it is working for [B3/S23](http://conwaylife.com/wiki/B3/S23) games.

These are the current milestones for version 0.1:

* Implemented (0.1.1)
    * Simulation runs for B3/S23 manually or with at a set interval
    * Patterns can be manually drawn on the board
    * Patterns can be loaded from RLE files
    * Cell size can be set
    * Grid lines can be optionally displayed
    * Patterns can be saved to RLE files
    * Iteration processor skips dead cell areas around active pattern for better performance
    * Repository and application have an explicit open source license
    * Repository is public!

* Milestone 0.1.2
    * Application has a bespoke icon
    * All text used in the application is localised
    * The name of the file that is currently opened is displayed
    * Patterns can be loaded from Life 1.05 files
    * Patterns can be loaded from Life 1.06 files
    * Patterns can be loaded from Plaintext (\*.cells) files

* Milestone 0.1.3
    * Iterations stop running when the pattern becomes static (still life)
    * Iteration processor detects that the pattern started to loop and user is informed
    * Patterns can be saved to Life 1.05 files
    * Patterns can be saved to Life 1.06 files
    * Patterns can be saved to Plaintext (\*.cells) files


## More information

For more information please see our [Wiki](https://bitbucket.org/ceottaki/game-of-life/wiki).


## Who do I talk to?

* Please feel free to [contact this repository's owner](https://bitbucket.org/account/notifications/send/?receiver=ceottaki)!