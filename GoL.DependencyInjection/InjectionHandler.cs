﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InjectionHandler.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.DependencyInjection
{
    using System;
    using System.Collections.Generic;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// Represents an object that handles dependency injection for this solution.
    /// </summary>
    public static class InjectionHandler
    {
        /// <summary>
        /// Initialises the dependencies.
        /// </summary>
        /// <param name="container">The dependency injection container.</param>
        public static void InitialiseDependencies(IUnityContainer container)
        {
            Infrastructure.ModuleInit.Initialise(container);
            Logic.ModuleInit.Initialise(container);
        }
    }
}
