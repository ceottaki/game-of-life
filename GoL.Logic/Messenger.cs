﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Messenger.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    using GoL.Logic.Annotations;

    /// <summary>
    /// Represents a generic messenger that can be used to send messages to the front-end.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated by dependency injection."), UsedImplicitly]
    internal class Messenger : IMessenger
    {
        /// <summary>
        /// Gets or sets the method used to send a message.
        /// </summary>
        /// <value>
        /// The method used to send a message.
        /// </value>
        /// <remarks>
        /// Parameters of the method are in this order: message, caption, message button and message type. The return value is the option selected.
        /// </remarks>
        public Func<string, string, MessageButton, MessageType, MessageResult> Send { get; set; }
    }
}
