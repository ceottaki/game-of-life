﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOfLifeProcessor.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;

    using GoL.Logic.Annotations;

    /// <summary>
    /// Represents a game of life processor.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated by dependency injection."), UsedImplicitly]
    internal class GameOfLifeProcessor : IGameOfLifeProcessor
    {
        private readonly ITextFilesRepository _repository;

        /// <summary>
        /// Initialises a new instance of the <see cref="GameOfLifeProcessor"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public GameOfLifeProcessor(ITextFilesRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Creates a new board with the given width and height.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>A board with the given width and height.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        public bool[,] CreateNewPattern(int width, int height)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException("width", width, "Value of width should be non-negative.");
            }

            if (height < 0)
            {
                throw new ArgumentOutOfRangeException("height", height, "Value of height should be non-negative.");
            }

            bool[,] newBoard = new bool[width, height];

            return newBoard;
        }

        /// <summary>
        /// Processes an iteration with a given current board.
        /// </summary>
        /// <param name="currentBoard">The current board.</param>
        /// <returns>The updated board after the iteration.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        public Board ProcessIteration(Board currentBoard)
        {
            if (null == currentBoard)
            {
                return null;
            }

            // TODO: Implement ways to detect and return the fact that the board is static and does not need processing any more.
            // TODO: Implement ways to detect and return the fact that the board is now looping with a certain period.
            bool[,] pattern = currentBoard.GetPattern();
            if (null == pattern)
            {
                return currentBoard;
            }

            int lengthX = pattern.GetLength(0);
            int lengthY = pattern.GetLength(1);
            int topX = Math.Max(currentBoard.TopX - 1, 0);
            int topY = Math.Max(currentBoard.TopY - 1, 0);
            int bottomX = Math.Min(currentBoard.BottomX + 2, lengthX);
            int bottomY = Math.Min(currentBoard.BottomY + 2, lengthY);

            bool[,] resultingPattern = new bool[lengthX, lengthY];

            int newTopX = bottomX - 1;
            int newTopY = bottomY - 1;
            int newBottomX = topX;
            int newBottomY = topY;
            for (int x = topX; x < bottomX; x++)
            {
                for (int y = topY; y < bottomY; y++)
                {
                    int numOfLiveNeighbours = GetNumberOfLiveNeighbours(pattern, x, y);
                    if ((numOfLiveNeighbours > 1) && (numOfLiveNeighbours < 4) && (pattern[x, y] || (3 == numOfLiveNeighbours)))
                    {
                        // Updates boundaries.
                        if (x < newTopX)
                        {
                            newTopX = x;
                        }

                        if (y < newTopY)
                        {
                            newTopY = y;
                        }

                        if (x > newBottomX)
                        {
                            newBottomX = x;
                        }

                        if (y > newBottomY)
                        {
                            newBottomY = y;
                        }

                        // Updates pattern.
                        resultingPattern[x, y] = true;
                    }
                    else
                    {
                        // Updates pattern.
                        resultingPattern[x, y] = false;
                    }
                }
            }

            // Corrects boundaries to account for a toroidal board.
            if (0 == newTopX)
            {
                newBottomX = lengthX - 1;
            }

            if (0 == newTopY)
            {
                newBottomY = lengthY - 1;
            }

            if ((lengthX - 1) == newBottomX)
            {
                newTopX = 0;
            }

            if ((lengthY - 1) == newBottomY)
            {
                newTopY = 0;
            }

            // Sets the new pattern and boundaries to the board.
            currentBoard.SetPattern(resultingPattern, newTopX, newTopY, newBottomX, newBottomY);
            return currentBoard;
        }

        /// <summary>
        /// Saves a given board to a file with the given file name.
        /// </summary>
        /// <param name="board">The board.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="trim">if set to <c>true</c> the board will be trimmed when saved to eliminate unnecessary dead cells.</param>
        /// <exception cref="System.ArgumentNullException">board; Value of <paramref name="board"/> cannot be null.</exception>
        public void SaveBoard(Board board, string fileName, GameOfLifeFileType fileType, bool trim)
        {
            if (null == board)
            {
                throw new ArgumentNullException("board", "Value of board cannot be null.");
            }

            if (fileType != GameOfLifeFileType.Rle)
            {
                throw new NotImplementedException("Only the RLE file type has been currently implemented.");
            }

            bool[,] pattern = board.GetPattern();
            bool[,] trimmedPattern = null;

            if (trim)
            {
                trimmedPattern = TrimPattern(pattern, board.TopX, board.TopY, board.BottomX, board.BottomY);
            }

            string patternName = Path.GetFileNameWithoutExtension(fileName);
            string fileContents;
            switch (fileType)
            {
                case GameOfLifeFileType.Life105:
                    // TODO: Implement method to save Life 1.05 file format.
                    throw new FormatException("File format not implemented.");

                case GameOfLifeFileType.Life105Or106:
                case GameOfLifeFileType.Life106:
                    // TODO: Implement method to save Life 1.05 file format.
                    throw new FormatException("File format not implemented.");

                case GameOfLifeFileType.Plaintext:
                    // TODO: Implement method to save Plaintext file format.
                    throw new FormatException("File format not implemented.");

                case GameOfLifeFileType.Rle:
                    fileContents = CreateRleContent(trim ? trimmedPattern : pattern, patternName);
                    break;

                default:
                    throw new FormatException("Unknown file format.");
            }

            if (fileContents != null)
            {
                _repository.SaveTextFile(fileContents, fileName);
            }
        }

        /// <summary>
        /// Loads a board from a file with the given file name.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="minBoardWidth">The minimum width of the board.</param>
        /// <param name="minBoardHeight">The minimum height of the board.</param>
        /// <returns>
        /// The loaded board.
        /// </returns>
        /// <remarks>
        /// If the file being loaded contains live cells that are larger than the given 
        /// <paramref name="minBoardWidth" /> and 
        /// <paramref name="minBoardHeight" /> the
        /// returned board will be as large as it needs to be to contain the board from the file; otherwise it will be as large as the given minimum values and the
        /// loaded board will be centralised in the result board.
        /// </remarks>
        /// <exception cref="System.IO.FileNotFoundException">File was not found when trying to load a text file.</exception>
        /// <exception cref="System.NotImplementedException">Only the RLE file type has been currently implemented.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">fileType; </exception>
        /// <exception cref="System.FormatException">
        /// There was a problem trying to open the given file likely due to the file being in the incorrect format.
        /// or
        /// Unknown file format.
        /// </exception>
        /// <exception cref="System.IO.IOException">There was a problem reading the given file from the disk.</exception>
        /// <exception cref="IndexOutOfRangeException">The RLE file being opened has an incorrect header specifying an incorrect size of the pattern.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        public Board LoadBoard(string fileName, GameOfLifeFileType fileType, int minBoardWidth, int minBoardHeight)
        {
            if (fileType != GameOfLifeFileType.Rle)
            {
                throw new NotImplementedException("Only the RLE file type has been currently implemented.");
            }

            bool[,] boardPattern = null;
            string fileContents = _repository.LoadTextFile(fileName);
            switch (fileType)
            {
                case GameOfLifeFileType.Life105:
                case GameOfLifeFileType.Life106:
                case GameOfLifeFileType.Life105Or106:
                    // TODO: Implement method to load Life 1.05 or Life 1.06 file format.
                    break;

                case GameOfLifeFileType.Plaintext:
                    // TODO: Implement method to load Plaintext file format.
                    break;

                case GameOfLifeFileType.Rle:
                    boardPattern = LoadRlePattern(fileContents);
                    break;

                default:
                    throw new FormatException("Unknown file format.");
            }

            if (null == boardPattern)
            {
                throw new FormatException("There was a problem trying to open the given file likely due to the file being in the incorrect format.");
            }

            int currentBoardWidth = boardPattern.GetLength(0);
            int currentBoardHeight = boardPattern.GetLength(1);
            int newBoardWidth = Math.Max(currentBoardWidth, minBoardWidth);
            int newBoardHeight = Math.Max(currentBoardHeight, minBoardHeight);

            Board resultBoard = new Board();
            if ((newBoardWidth <= currentBoardWidth) && (newBoardHeight <= currentBoardHeight))
            {
                resultBoard.SetPattern(boardPattern);
                return resultBoard;
            }

            bool[,] paddedResult = new bool[newBoardWidth, newBoardHeight];
            int widthPadding = (newBoardWidth - currentBoardWidth) / 2;
            int heightPadding = (newBoardHeight - currentBoardHeight) / 2;

            for (int x = 0; x < currentBoardWidth; x++)
            {
                for (int y = 0; y < currentBoardHeight; y++)
                {
                    paddedResult[x + widthPadding, y + heightPadding] = boardPattern[x, y];
                }
            }

            boardPattern = paddedResult;

            resultBoard.SetPattern(boardPattern);
            return resultBoard;
        }

        /// <summary>
        /// Gets the number of live neighbours of a cell in the given position in the given board.
        /// </summary>
        /// <param name="currentPattern">The current pattern.</param>
        /// <param name="positionX">The X position of the cell to be checked.</param>
        /// <param name="positionY">The Y position of the cell to be checked.</param>
        /// <returns>The number of live neighbours of the cell in the given position.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static int GetNumberOfLiveNeighbours(bool[,] currentPattern, int positionX, int positionY)
        {
            int result = 0;

            for (int x = positionX - 1; x <= positionX + 1; x++)
            {
                for (int y = positionY - 1; y <= positionY + 1; y++)
                {
                    if (((x != positionX) || (y != positionY))
                        && currentPattern[(x + currentPattern.GetLength(0)) % currentPattern.GetLength(0), (y + currentPattern.GetLength(1)) % currentPattern.GetLength(1)])
                    {
                        result++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Trims a given pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <param name="topX">The top X coordinate that will be included in the trimmed result.</param>
        /// <param name="topY">The top Y coordinate that will be included in the trimmed result.</param>
        /// <param name="bottomX">The bottom X coordinate that will be included in the trimmed result.</param>
        /// <param name="bottomY">The bottom Y coordinate that will be included in the trimmed result.</param>
        /// <returns>
        /// The trimmed pattern.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space."),
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space."),
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static bool[,] TrimPattern(bool[,] pattern, int topX, int topY, int bottomX, int bottomY)
        {
            if (null == pattern)
            {
                return null;
            }

            int finalWidth = bottomX - topX + 1;
            int finalHeight = bottomY - topY + 1;

            bool[,] result = new bool[finalWidth, finalHeight];

            for (int y = topY; y < (topY + finalHeight); y++)
            {
                for (int x = topX; x < (topX + finalWidth); x++)
                {
                    result[x - topX, y - topY] = pattern[x, y];
                }
            }

            return result;
        }

        /// <summary>
        /// Creates the content for an RLE file based on the given pattern.
        /// </summary>
        /// <param name="boardPattern">The board pattern.</param>
        /// <param name="patternName">The name of the pattern.</param>
        /// <returns>
        /// The contents in RLE format that represents the given pattern.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">boardPattern; Value of board cannot be null.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static string CreateRleContent(bool[,] boardPattern, string patternName)
        {
            if (null == boardPattern)
            {
                throw new ArgumentNullException("boardPattern", "Value of boardPattern cannot be null.");
            }

            const string Rule = "B3/S23";
            int width = boardPattern.GetLength(0);
            int height = boardPattern.GetLength(1);

            // Builds the file in memory first.
            StringBuilder stringBuilder = new StringBuilder();

            // Create header.
            stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "#N {0}{1}", patternName, Environment.NewLine);
            stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "x = {0}, y = {1}, rule = {2}{3}", width, height, Rule, Environment.NewLine);

            // Creates pattern description.
            bool currentVal = boardPattern[0, 0];
            int currentCount = 0;
            int currentLineLength = 0;
            for (int y = 0; y < height; y++)
            {
                string currentLine;
                for (int x = 0; x < width; x++)
                {
                    if (boardPattern[x, y] == currentVal)
                    {
                        currentCount++;
                    }
                    else
                    {
                        currentLine = (currentCount > 1) ? string.Format(CultureInfo.CurrentCulture, "{0}{1}", currentCount, currentVal ? "o" : "b") : (currentVal ? "o" : "b");
                        currentLineLength += currentLine.Length;
                        if (currentLineLength > 70)
                        {
                            stringBuilder.AppendLine();
                            currentLineLength = currentLine.Length;
                        }

                        stringBuilder.Append(currentLine);
                        currentCount = 1;
                        currentVal = boardPattern[x, y];
                    }
                }

                currentLine = (currentCount > 1) ? string.Format(CultureInfo.CurrentCulture, "{0}{1}", currentCount, currentVal ? "o" : "b") : (currentVal ? "o" : "b");
                currentLineLength += currentLine.Length;
                if (currentLineLength > 70)
                {
                    stringBuilder.AppendLine();
                    currentLineLength = currentLine.Length;
                }

                stringBuilder.Append(currentLine);

                if (currentLineLength == 70)
                {
                    stringBuilder.AppendLine();
                    currentLineLength = 0;
                }

                currentCount = 0;
                if ((height - 1) == y)
                {
                    stringBuilder.AppendLine("!");
                    currentLineLength = 0;
                }
                else
                {
                    currentLineLength++;
                    stringBuilder.Append("$");
                    currentVal = boardPattern[0, y + 1];
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Loads a pattern from the contents of an RLE-formatted file.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        /// <returns>The loaded pattern.</returns>
        /// <exception cref="System.FormatException">There was a problem trying to open the given file likely due to the file being in the incorrect format.</exception>
        /// <exception cref="System.IndexOutOfRangeException">The RLE file being opened has an incorrect header specifying an incorrect size of the pattern.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space."),
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static bool[,] LoadRlePattern(string fileContents)
        {
            // TODO: Improve readability of method that loads an RLE file and improving its efficiency, possibly by interpreting the pattern description as running through lines.
            StringBuilder stringBuilder = new StringBuilder();
            int patternWidth = 0;
            int patternHeight = 0;

            // Unify line feed characters into \n.
            fileContents = fileContents.Replace("\r\n", "\n").Replace('\r', '\n');
            string[] fileLines = fileContents.Split('\n');

            // Run through the lines of the file processing headers and eliminating line breaks for the pattern description.
            foreach (string line in fileLines)
            {
                string trimmedLine = line.Trim();
                if (!trimmedLine.StartsWith("#", StringComparison.OrdinalIgnoreCase))
                {
                    string[] headerSplit = trimmedLine.Split('=', ',');
                    if (1 == headerSplit.Length)
                    {
                        // Line is not a header. Ignores everything after the exclamation point if present and adds the contents to the pattern description.
                        trimmedLine = trimmedLine.Split('!')[0];
                        stringBuilder.Append(trimmedLine);
                    }
                    else
                    {
                        // Line is a header.
                        for (int i = 0; i < headerSplit.Length; i++)
                        {
                            if ((i + 1) < headerSplit.Length)
                            {
                                switch (headerSplit[i].Trim().ToUpperInvariant())
                                {
                                    case "X":
                                        if (!int.TryParse(headerSplit[i + 1], out patternWidth))
                                        {
                                            throw new FormatException("There was a problem trying to open the given file likely due to the file being in the incorrect format.");
                                        }

                                        break;

                                    case "Y":
                                        if (!int.TryParse(headerSplit[i + 1], out patternHeight))
                                        {
                                            throw new FormatException("There was a problem trying to open the given file likely due to the file being in the incorrect format.");
                                        }

                                        break;

                                    case "RULE":
                                        if (("B3/S23" != headerSplit[i + 1].Trim().ToUpperInvariant()) && ("3/23" != headerSplit[i + 1].Trim().ToUpperInvariant()))
                                        {
                                            throw new FormatException(
                                                "There was a problem trying to open the given file due to the file describing a pattern for a rule other than B3/S23.");
                                        }

                                        break;
                                }
                            }
                        }
                    }
                }
            }

            if ((0 == patternWidth) && (0 == patternHeight))
            {
                throw new FormatException("There was a problem trying to open the given file due to an incorrect header.");
            }

            // Interprets the RLE pattern description into a multidimensional array as it is used by this system.
            bool[,] result = new bool[patternWidth, patternHeight];
            string patternDescription = stringBuilder.ToString().ToUpperInvariant();
            string numberAccumulator = string.Empty;
            int x = 0;
            int y = 0;
            foreach (char patternCharacter in patternDescription)
            {
                if (char.IsDigit(patternCharacter))
                {
                    numberAccumulator += patternCharacter;
                }
                else
                {
                    int runCount;
                    if (!int.TryParse(numberAccumulator, out runCount))
                    {
                        runCount = 1;
                    }

                    switch (patternCharacter)
                    {
                        case 'B':
                            x += runCount;
                            break;

                        case 'O':
                            for (int posX = x; posX < (x + runCount); posX++)
                            {
                                try
                                {
                                    result[posX, y] = true;
                                }
                                catch (IndexOutOfRangeException exception)
                                {
                                    // TODO: Replace exception raised with a bespoke specific exception that will inform the user the exact message below without using a reserved exception.
                                    throw new IndexOutOfRangeException("The RLE file being opened has an incorrect header specifying an incorrect size of the pattern.", exception);
                                }
                            }

                            x += runCount;
                            break;

                        case '$':
                            x = 0;
                            y++;
                            break;
                    }

                    numberAccumulator = string.Empty;
                }
            }

            return result;
        }
    }
}
