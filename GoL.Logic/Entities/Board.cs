﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Board.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a game of life board.
    /// </summary>
    public class Board
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private bool[,] _pattern;

        /// <summary>
        /// Gets the top X coordinate that contains a live cell in the board's pattern.
        /// </summary>
        /// <value>
        /// The top X coordinate that contains a live cell in the board's pattern.
        /// </value>
        public int TopX { get; private set; }

        /// <summary>
        /// Gets the top Y coordinate that contains a live cell in the board's pattern.
        /// </summary>
        /// <value>
        /// The top Y coordinate that contains a live cell in the board's pattern.
        /// </value>
        public int TopY { get; private set; }

        /// <summary>
        /// Gets the bottom X coordinate that contains a live cell in the board's pattern.
        /// </summary>
        /// <value>
        /// The bottom X coordinate that contains a live cell in the board's pattern.
        /// </value>
        public int BottomX { get; private set; }

        /// <summary>
        /// Gets the bottom Y coordinate that contains a live cell in the board's pattern.
        /// </summary>
        /// <value>
        /// The bottom Y coordinate that contains a live cell in the board's pattern.
        /// </value>
        public int BottomY { get; private set; }

        /// <summary>
        /// Gets the pattern currently in the board.
        /// </summary>
        /// <returns>The pattern currently in the board.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        public bool[,] GetPattern()
        {
            return _pattern != null ? (bool[,])_pattern.Clone() : null;
        }

        /// <summary>
        /// Sets the pattern to the board.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        public void SetPattern(bool[,] pattern)
        {
            if (null == pattern)
            {
                _pattern = null;
                return;
            }

            _pattern = (bool[,])pattern.Clone();

            // Calculates the boundaries.
            int lengthX = pattern.GetLength(0);
            int lengthY = pattern.GetLength(1);

            int topX = lengthX - 1;
            int topY = lengthY - 1;
            int bottomX = 0;
            int bottomY = 0;

            for (int x = 0; x < lengthX; x++)
            {
                for (int y = 0; y < lengthY; y++)
                {
                    if (pattern[x, y])
                    {
                        if (x < topX)
                        {
                            topX = x;
                        }

                        if (y < topY)
                        {
                            topY = y;
                        }

                        if (x > bottomX)
                        {
                            bottomX = x;
                        }

                        if (y > bottomY)
                        {
                            bottomY = y;
                        }
                    }
                }
            }

            // Corrects boundaries to account for a toroidal board.
            if (0 == topX)
            {
                bottomX = lengthX - 1;
            }

            if (0 == topY)
            {
                bottomY = lengthY - 1;
            }

            if ((lengthX - 1) == bottomX)
            {
                topX = 0;
            }

            if ((lengthY - 1) == bottomY)
            {
                topY = 0;
            }

            // Sets the boundaries.
            this.TopX = topX;
            this.TopY = topY;
            this.BottomX = bottomX;
            this.BottomY = bottomY;
        }

        /// <summary>
        /// Sets the pattern with the given boundaries as well.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <param name="topX">The top X of the pattern boundary.</param>
        /// <param name="topY">The top Y of the pattern boundary.</param>
        /// <param name="bottomX">The bottom X of the pattern boundary.</param>
        /// <param name="bottomY">The bottom Y of the pattern boundary.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        internal void SetPattern(bool[,] pattern, int topX, int topY, int bottomX, int bottomY)
        {
            if (null == pattern)
            {
                _pattern = null;
                return;
            }

            _pattern = (bool[,])pattern.Clone();
            this.TopX = topX;
            this.TopY = topY;
            this.BottomX = bottomX;
            this.BottomY = bottomY;
        }
    }
}
