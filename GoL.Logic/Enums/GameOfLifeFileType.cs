﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOfLifeFileType.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Specifies the type of file used to store a game of life board.
    /// </summary>
    public enum GameOfLifeFileType
    {
        /// <summary>
        /// The Life 1.05 file format (*.lif or *.life).
        /// </summary>
        /// <remarks>See http://www.conwaylife.com/wiki/Life_1.05 for specification.</remarks>
        Life105,

        /// <summary>
        /// The Life 1.06 file format (*.lif or *.life).
        /// </summary>
        /// <remarks>See http://www.conwaylife.com/wiki/Life_1.06 for specification.</remarks>
        Life106,

        /// <summary>
        /// Either Life 1.05 or 1.06. This is used to open a file as they have the same extension.
        /// </summary>
        Life105Or106,

        /// <summary>
        /// The Plaintext file format (*.cells).
        /// </summary>
        /// <remarks>See http://www.conwaylife.com/wiki/Plaintext for specification.</remarks>
        Plaintext,

        /// <summary>
        /// The Run Length Encoded file format (*.rle).
        /// </summary>
        /// <remarks>See http://www.conwaylife.com/wiki/RLE for specification.</remarks>
        Rle,
    }
}
