﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageResult.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Specifies which button or choice was selected for acknowledging a message.
    /// </summary>
    /// <remarks>
    /// The values in this enumeration are compatible with <see cref="T:System.Windows.MessageBoxResult" />.
    /// </remarks>
    public enum MessageResult
    {
        /// <summary>
        /// No option was selected.
        /// </summary>
        None = 0,

        /// <summary>
        /// The OK option was selected.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// The cancel option was selected.
        /// </summary>
        Cancel = 2,

        /// <summary>
        /// The yes option was selected.
        /// </summary>
        Yes = 6,

        /// <summary>
        /// The no option was selected.
        /// </summary>
        No = 7,
    }
}
