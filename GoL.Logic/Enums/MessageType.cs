﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageType.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Specifies the type of message to be sent.
    /// </summary>
    /// <remarks>
    /// The values in this enumeration are compatible with <see cref="T:System.Windows.MessageBoxImage" />.
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags", Justification = "Values of this enumeration cannot be combined.")]
    public enum MessageType
    {
        /// <summary>
        /// No type has been specified.
        /// </summary>
        None = 0,

        /// <summary>
        /// An error message.
        /// </summary>
        Error = 16,

        /// <summary>
        /// A question.
        /// </summary>
        Question = 32,

        /// <summary>
        /// A warning message.
        /// </summary>
        Warning = 48,

        /// <summary>
        /// An informational message.
        /// </summary>
        Information = 64,
    }
}
