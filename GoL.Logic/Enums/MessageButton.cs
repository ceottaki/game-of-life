﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageButton.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Specifies the combination of buttons to be used for a message.
    /// </summary>
    /// <remarks>
    /// The values in this enumeration are compatible with <see cref="T:System.Windows.MessageBoxButton"/>.
    /// </remarks>
    public enum MessageButton
    {
        /// <summary>
        /// Use only the OK button.
        /// </summary>
        Ok = 0,

        /// <summary>
        /// Use buttons OK and Cancel.
        /// </summary>
        OkCancel = 1,

        /// <summary>
        /// Use buttons Yes, No and Cancel.
        /// </summary>
        YesNoCancel = 3,

        /// <summary>
        /// Use buttons Yes and No.
        /// </summary>
        YesNo = 4,
    }
}
