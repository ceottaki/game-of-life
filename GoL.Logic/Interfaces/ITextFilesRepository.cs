﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITextFilesRepository.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines properties and methods for an implementation of a game of life repository.
    /// </summary>
    public interface ITextFilesRepository
    {
        /// <summary>
        /// Saves a text file with the given contents to the given file name.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <exception cref="System.ArgumentNullException">fileName; Value of fileName cannot be null or empty.</exception>
        void SaveTextFile(string fileContents, string fileName);

        /// <summary>
        /// Loads a text file with the given file name.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <returns>The contents of the loaded file.</returns>
        /// <exception cref="System.IO.FileNotFoundException">File was not found when trying to load a text file.</exception>
        string LoadTextFile(string fileName);
    }
}
