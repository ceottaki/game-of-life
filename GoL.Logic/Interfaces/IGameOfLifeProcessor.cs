﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGameOfLifeProcessor.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Logic
{
    /// <summary>
    /// Defines properties and methods for an implementation of a game of life processor.
    /// </summary>
    public interface IGameOfLifeProcessor
    {
        /// <summary>
        /// Creates a new empty pattern with the given width and height.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>An empty pattern with the given width and height.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        bool[,] CreateNewPattern(int width, int height);

        /// <summary>
        /// Processes an iteration with a given current board.
        /// </summary>
        /// <param name="currentBoard">The current board.</param>
        /// <returns>The updated board after the iteration.</returns>
        Board ProcessIteration(Board currentBoard);

        /// <summary>
        /// Saves a given board to a file with the given file name.
        /// </summary>
        /// <param name="board">The board.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="trim">if set to <c>true</c> the board will be trimmed when saved to eliminate unnecessary dead cells.</param>
        void SaveBoard(Board board, string fileName, GameOfLifeFileType fileType, bool trim);

        /// <summary>
        /// Loads a board from a file with the given file name.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="minBoardWidth">The minimum width of the board.</param>
        /// <param name="minBoardHeight">The minimum height of the board.</param>
        /// <returns>
        /// The loaded board.
        /// </returns>
        /// <remarks>
        /// If the file being loaded contains live cells that are larger than the given <paramref name="minBoardWidth"/> and <paramref name="minBoardHeight"/> the
        /// returned board will be as large as it needs to be to contain the board from the file; otherwise it will be as large as the given minimum values and the
        /// loaded board will be centralised in the result board.
        /// </remarks>
        Board LoadBoard(string fileName, GameOfLifeFileType fileType, int minBoardWidth, int minBoardHeight);
    }
}