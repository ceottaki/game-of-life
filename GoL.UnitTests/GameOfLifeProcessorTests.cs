﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOfLifeProcessorTests.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using GoL.Logic;
    using GoL.UnitTests.Properties;

    using Microsoft.Practices.Unity;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Represents a set of tests for the game of life processor.
    /// </summary>
    [TestFixture]
    public class GameOfLifeProcessorTests
    {
        private readonly Random _rnd = new Random();
        private readonly object _lockObject = new object();
        private IGameOfLifeProcessor _gameOfLifeProcessor;
        private Mock<ITextFilesRepository> _textFilesRepositoryMock;
        private string _fileContents;

        /// <summary>
        /// Initialises this instance of the test fixture before every test.
        /// </summary>
        [SetUp]
        public void Init()
        {
            using (IUnityContainer container = new UnityContainer())
            {
                this.SetUpMockRepository();
                container.RegisterInstance(_textFilesRepositoryMock.Object);
                ModuleInit.Initialise(container);
                _gameOfLifeProcessor = container.Resolve<IGameOfLifeProcessor>();
            }
        }

        /// <summary>
        /// Finalises this instance of the test fixture after every test.
        /// </summary>
        [TearDown]
        public void Finalise()
        {
            _gameOfLifeProcessor = null;
            _textFilesRepositoryMock = null;
        }

        /// <summary>
        /// Tests that the size of a new pattern created is correct.
        /// </summary>
        [Test]
        public void TestCreateNewPatternReturnsCorrectSize()
        {
            int width = _rnd.Next(1, 1025);
            int height = _rnd.Next(1, 1025);

            bool[,] result = _gameOfLifeProcessor.CreateNewPattern(width, height);

            Assert.IsTrue(
                (result.GetLength(0) == width) && (result.GetLength(1) == height),
                string.Format(CultureInfo.CurrentCulture, Resources.TestCreateNewPatternReturnsCorrectSizeFailMessage, width, height, result.GetLength(0), result.GetLength(1)));
        }

        /// <summary>
        /// Tests that the size of a new pattern created with width and height of zero also zero.
        /// </summary>
        [Test]
        public void TestCreateNewPatternWithZeroSize()
        {
            bool[,] result = _gameOfLifeProcessor.CreateNewPattern(0, 0);

            Assert.AreEqual(0, result.Length, string.Format(CultureInfo.CurrentCulture, Resources.TestCreateNewPatternWithZeroSizeFailMessage, result.Length));
        }

        /// <summary>
        /// Tests that calling create new pattern with a negative width throws an exception.
        /// </summary>
        [Test]
        public void TestCreateNewPatternWithNegativeWidthThrowsException()
        {
            int width = _rnd.Next(1, 1025) * -1;
            int height = _rnd.Next(1, 1025);

            Assert.Throws<ArgumentOutOfRangeException>(
                () => _gameOfLifeProcessor.CreateNewPattern(width, height),
                Resources.TestCreateNewPatternWithNegativeWidthThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that calling create new pattern with a negative height throws an exception.
        /// </summary>
        [Test]
        public void TestCreateNewPatternWithNegativeHeightThrowsException()
        {
            int width = _rnd.Next(1, 1025);
            int height = _rnd.Next(1, 1025) * -1;

            Assert.Throws<ArgumentOutOfRangeException>(
                () => _gameOfLifeProcessor.CreateNewPattern(width, height),
                Resources.TestCreateNewPatternWithNegativeHeightThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with a null board returns null.
        /// </summary>
        [Test]
        public void TestProcessIterationWithNullBoardReturnsNull()
        {
            Board result = _gameOfLifeProcessor.ProcessIteration(null);

            Assert.IsNull(result, Resources.TestProcessIterationWithNullBoardReturnsNullFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with a null pattern returns the same board.
        /// </summary>
        [Test]
        public void TestProcessIterationWithNullPatternReturnsSameBoard()
        {
            Board boardNullPattern = new Board();
            Board result = _gameOfLifeProcessor.ProcessIteration(boardNullPattern);

            Assert.AreSame(boardNullPattern, result, Resources.TestProcessIterationWithNullPatternReturnsSameBoardFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration returns a pattern with the same size as the original pattern.
        /// </summary>
        [Test]
        public void TestProcessIterationReturnsSameSizePattern()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            currentBoard.SetPattern(GetGliderPattern(width, height));
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();

            Assert.IsTrue(
                (result.GetLength(0) == width) && (result.GetLength(1) == height),
                string.Format(CultureInfo.CurrentCulture, Resources.TestProcessIterationReturnsSameSizePatternFailMessage, width, height, result.GetLength(0), result.GetLength(1)));
        }

        /// <summary>
        /// Tests that processing an iteration with an empty pattern returns an empty pattern.
        /// </summary>
        [Test]
        public void TestProcessIterationWithEmptyPatternReturnsEmptyPattern()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            currentBoard.SetPattern(GetEmptyPattern(width, height));
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyTrue = false;

            for (int x = 0; x < result.GetLength(0); x++)
            {
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    anyTrue = anyTrue || result[x, y];
                }
            }

            Assert.IsFalse(anyTrue, Resources.TestProcessIterationWithEmptyPatternReturnsEmptyPatternFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with all live cells with no neighbours returns an empty pattern.
        /// </summary>
        [Test]
        public void TestProcessIterationWithLiveCellsWithNoNeighboursReturnsEmptyPattern()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = GetEmptyPattern(width, height);
            for (int x = 1; x < (width - 1); x = x + 2)
            {
                for (int y = 1; y < (height - 1); y = y + 2)
                {
                    pattern[x, y] = true;
                }
            }

            currentBoard.SetPattern(pattern);
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyTrue = false;

            for (int x = 0; x < result.GetLength(0); x++)
            {
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    anyTrue = anyTrue || result[x, y];
                }
            }

            Assert.IsFalse(anyTrue, Resources.TestProcessIterationWithLiveCellsWithNoNeighboursReturnsEmptyPatternFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with all live cells with exactly one neighbour returns an empty pattern.
        /// </summary>
        [Test]
        public void TestProcessIterationWithLiveCellsWithOneNeighbourReturnsEmptyPattern()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = GetEmptyPattern(width, height);
            for (int x = 1; x < (width - 2); x = x + 3)
            {
                for (int y = 1; y < (height - 1); y = y + 2)
                {
                    pattern[x, y] = true;
                    pattern[x + 1, y] = true;
                }
            }

            currentBoard.SetPattern(pattern);
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyTrue = false;

            for (int x = 0; x < result.GetLength(0); x++)
            {
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    anyTrue = anyTrue || result[x, y];
                }
            }

            Assert.IsFalse(anyTrue, Resources.TestProcessIterationWithLiveCellsWithOneNeighbourReturnsEmptyPatternFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with all live cells with exactly two neighbours returns the exact same pattern.
        /// </summary>
        [Test]
        public void TestProcessIterationWithLiveCellsWithTwoNeighboursReturnsSamePattern()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = GetEmptyPattern(width, height);
            for (int x = 1; x < (width - 2); x = x + 3)
            {
                for (int y = 1; y < (height - 2); y = y + 3)
                {
                    pattern[x, y] = true;
                    pattern[x + 1, y] = true;
                    pattern[x, y + 1] = true;
                    pattern[x + 1, y + 1] = true;
                }
            }

            currentBoard.SetPattern(pattern);
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyDifferent = false;

            for (int x = 0; x < result.GetLength(0); x++)
            {
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    anyDifferent = anyDifferent || (result[x, y] != pattern[x, y]);
                }
            }

            Assert.IsFalse(anyDifferent, Resources.TestProcessIterationWithLiveCellsWithTwoNeighboursReturnsSamePatternFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with certain live cells with more than three neighbours returns a pattern where those particular cells have died.
        /// </summary>
        [Test]
        public void TestProcessIterationWithLiveCellsWithMoreThanThreeNeighboursReturnsNewDeadCells()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = GetEmptyPattern(width, height);
            for (int x = 2; x < (width - 2); x = x + 4)
            {
                for (int y = 2; y < (height - 2); y = y + 4)
                {
                    pattern[x, y] = true;
                    pattern[x - 1, y] = true;
                    pattern[x + 1, y] = true;
                    pattern[x, y - 1] = true;
                    pattern[x, y + 1] = true;
                }
            }

            currentBoard.SetPattern(pattern);
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyIncorrect = false;

            for (int x = 2; x < (width - 2); x = x + 4)
            {
                for (int y = 2; y < (height - 2); y = y + 4)
                {
                    anyIncorrect = anyIncorrect || result[x, y];
                }
            }

            Assert.IsFalse(anyIncorrect, Resources.TestProcessIterationWithLiveCellsWithMoreThanThreeNeighboursReturnsNewDeadCellsFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with certain dead cells with exactly three neighbours returns a pattern where those particular cells are new live cells.
        /// </summary>
        [Test]
        public void TestProcessIterationWithDeadCellsWithThreeNeighboursReturnsNewLiveCells()
        {
            int width = _rnd.Next(3, 1025);
            int height = _rnd.Next(3, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = GetEmptyPattern(width, height);
            for (int x = 2; x < (width - 2); x = x + 4)
            {
                for (int y = 2; y < (height - 1); y = y + 3)
                {
                    pattern[x - 1, y] = true;
                    pattern[x + 1, y] = true;
                    pattern[x, y - 1] = true;
                }
            }

            currentBoard.SetPattern(pattern);
            Board resultBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            bool[,] result = resultBoard.GetPattern();
            bool anyIncorrect = false;

            for (int x = 2; x < (width - 2); x = x + 4)
            {
                for (int y = 2; y < (height - 1); y = y + 3)
                {
                    anyIncorrect = anyIncorrect || (!result[x, y]);
                }
            }

            Assert.IsFalse(anyIncorrect, Resources.TestProcessIterationWithDeadCellsWithThreeNeighboursReturnsNewLiveCellsFailMessage);
        }

        /// <summary>
        /// Tests that processing an iteration with a glider pattern positions the pattern correctly.
        /// </summary>
        /// <remarks>
        /// This test will get a glider pattern and run 4 iterations to it to see if the same pattern can be found only dislocated by (1,1).
        /// </remarks>
        [Test]
        public void TestProcessIterationWithGliderPattern()
        {
            bool[,] gliderPattern = GetGliderPattern(11, 11);
            Board currentBoard = new Board();
            currentBoard.SetPattern(gliderPattern);
            for (int i = 0; i < 4; i++)
            {
                currentBoard = _gameOfLifeProcessor.ProcessIteration(currentBoard);
            }

            bool[,] newGliderPattern = currentBoard.GetPattern();

            // Makes sure the new glider pattern is correctly positioned.
            bool anyIncorrect = !(newGliderPattern[6, 5] && newGliderPattern[7, 6] && newGliderPattern[5, 7] && newGliderPattern[6, 7] && newGliderPattern[7, 7]);

            if (!anyIncorrect)
            {
                // Removes the new glider pattern.
                newGliderPattern[6, 5] = false;
                newGliderPattern[7, 6] = false;
                newGliderPattern[5, 7] = false;
                newGliderPattern[6, 7] = false;
                newGliderPattern[7, 7] = false;

                // Makes sure all the board is now empty.
                for (int x = 0; x < newGliderPattern.GetLength(0); x++)
                {
                    for (int y = 0; y < newGliderPattern.GetLength(1); y++)
                    {
                        anyIncorrect = anyIncorrect || newGliderPattern[x, y];
                    }
                }
            }

            Assert.IsFalse(anyIncorrect, Resources.TestProcessIterationWithGliderPatternFailMessage);
        }

        /// <summary>
        /// Tests that getting a pattern from the board returns the set pattern when no iterations have been run.
        /// </summary>
        [Test]
        public void TestBoardGetPatternReturnsSetPattern()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = this.GetRandomPattern(width, height, 0);
            currentBoard.SetPattern(pattern);
            bool[,] result = currentBoard.GetPattern();
            bool anyDifferent = false;

            for (int x = 0; x < result.GetLength(0); x++)
            {
                for (int y = 0; y < result.GetLength(1); y++)
                {
                    anyDifferent = anyDifferent || (result[x, y] != pattern[x, y]);
                }
            }

            Assert.IsFalse(anyDifferent, Resources.TestBoardGetPatternReturnsSetPatternFailMessage);
        }

        /// <summary>
        /// Tests that getting a pattern from the board after setting the pattern to null returns null.
        /// </summary>
        [Test]
        public static void TestBoardGetPatternReturnNullPatternWhenSetPatternToNull()
        {
            Board currentBoard = new Board();
            currentBoard.SetPattern(null);
            bool[,] result = currentBoard.GetPattern();

            Assert.IsNull(result, Resources.TestBoardGetPatternReturnNullPatternWhenSetPatternToNullFailMessage);
        }

        /// <summary>
        /// Tests that everything to the top and to the left of the top-left boundary is dead when setting a pattern.
        /// </summary>
        [Test]
        public void TestBoardTopLeftBoundaryDeadZoneOnSettingPattern()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = this.GetRandomPattern(width, height, 5);
            currentBoard.SetPattern(pattern);
            bool anyIncorrect = false;

            for (int x = currentBoard.TopX - 1; x >= 0; x--)
            {
                for (int y = currentBoard.TopY - 1; y >= 0; y--)
                {
                    anyIncorrect = anyIncorrect || pattern[x, y];
                }
            }

            Assert.IsFalse(anyIncorrect, Resources.TestBoardTopLeftBoundaryOnSettingPatternFailMessage);
        }

        /// <summary>
        /// Tests that the row and column of the top-left boundary have at least one alive cell when setting a pattern.
        /// </summary>
        [Test]
        public void TestBoardTopLeftBoundaryAliveCellOnSettingPattern()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = this.GetRandomPattern(width, height, 5);
            currentBoard.SetPattern(pattern);

            bool allDeadX = true;
            for (int x = 0; x < width; x++)
            {
                allDeadX = allDeadX && (!pattern[x, currentBoard.TopY]);
            }

            bool allDeadY = true;
            for (int y = 0; y < height; y++)
            {
                allDeadY = allDeadY && (!pattern[currentBoard.TopX, y]);
            }

            Assert.IsFalse(allDeadX || allDeadY, Resources.TestBoardTopLeftBoundaryAliveCellOnSettingPatternFailMessage);
        }

        /// <summary>
        /// Tests that everything to the bottom and to the right of the bottom-right boundary is dead when setting a pattern.
        /// </summary>
        [Test]
        public void TestBoardBottomRightBoundaryDeadZoneOnSettingPattern()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = this.GetRandomPattern(width, height, 5);
            currentBoard.SetPattern(pattern);
            bool anyIncorrect = false;

            for (int x = currentBoard.BottomX + 1; x < width; x++)
            {
                for (int y = currentBoard.BottomY + 1; y < height; y++)
                {
                    anyIncorrect = anyIncorrect || pattern[x, y];
                }
            }

            Assert.IsFalse(anyIncorrect, Resources.TestBoardBottomRightBoundaryDeadZoneOnSettingPatternFailMessage);
        }

        /// <summary>
        /// Tests that the row and column of the bottom-right boundary have at least one alive cell when setting a pattern.
        /// </summary>
        [Test]
        public void TestBoardBottomRightBoundaryAliveCellOnSettingPattern()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);

            Board currentBoard = new Board();
            bool[,] pattern = this.GetRandomPattern(width, height, 5);
            currentBoard.SetPattern(pattern);

            bool allDeadX = true;
            for (int x = 0; x < width; x++)
            {
                allDeadX = allDeadX && (!pattern[x, currentBoard.BottomY]);
            }

            bool allDeadY = true;
            for (int y = 0; y < height; y++)
            {
                allDeadY = allDeadY && (!pattern[currentBoard.BottomX, y]);
            }

            Assert.IsFalse(allDeadX || allDeadY, Resources.TestBoardBottomRightBoundaryAliveCellOnSettingPatternFailMessage);
        }

        /// <summary>
        /// Tests that the boundary adjustments made after processing iteration when the pattern gets to the bottom-right corner are correct.
        /// </summary>
        [Test]
        public void TestBoundaryAdjustmentAfterProcessingIterationPatternGetsBottomRightCorner()
        {
            bool[,] gliderPattern = GetGliderPattern(13, 15);
            Board currentBoard = new Board();
            currentBoard.SetPattern(gliderPattern);

            for (int i = 0; i < 22; i++)
            {
                _gameOfLifeProcessor.ProcessIteration(currentBoard);
            }

            Assert.IsTrue(
                (0 == currentBoard.TopX) && (0 == currentBoard.TopY) && (12 == currentBoard.BottomX) && (14 == currentBoard.BottomY),
                Resources.TestBoundaryAdjustmentAfterProcessingIterationPatternGetsBottomRightCornerFailMessage);
        }

        /// <summary>
        /// Tests that the boundary adjustments made after processing iteration when the pattern gets to the top-left corner are correct.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        [Test]
        public void TestBoundaryAdjustmentAfterProcessingIterationPatternGetsTopLeftCorner()
        {
            bool[,] gliderPattern = new bool[13, 15];
            int widthPadding = (int)Math.Floor(10 / 2.0m);
            int heightPadding = (int)Math.Floor(12 / 2.0m);

            gliderPattern[0 + widthPadding, 0 + heightPadding] = true;
            gliderPattern[1 + widthPadding, 0 + heightPadding] = true;
            gliderPattern[2 + widthPadding, 0 + heightPadding] = true;
            gliderPattern[0 + widthPadding, 1 + heightPadding] = true;
            gliderPattern[1 + widthPadding, 2 + heightPadding] = true;

            Board currentBoard = new Board();
            currentBoard.SetPattern(gliderPattern);

            for (int i = 0; i < 21; i++)
            {
                _gameOfLifeProcessor.ProcessIteration(currentBoard);
            }

            Assert.IsTrue(
                (0 == currentBoard.TopX) && (0 == currentBoard.TopY) && (12 == currentBoard.BottomX) && (14 == currentBoard.BottomY),
                Resources.TestBoundaryAdjustmentAfterProcessingIterationPatternGetsTopLeftCornerFailMessage);
        }

        /// <summary>
        /// Tests that saving a board with a null board throws an exception.
        /// </summary>
        [Test]
        public void TestSaveBoardWithNullBoardThrowsException()
        {
            Assert.Throws<ArgumentNullException>(
                () => _gameOfLifeProcessor.SaveBoard(null, "Glider.rle", GameOfLifeFileType.Rle, true),
                Resources.TestSaveBoardWithNullBoardThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that saving a board with a file type different than RLE throws an exception.
        /// </summary>
        /// <remarks>
        /// This test should be removed once other file types get implemented.
        /// </remarks>
        [Test]
        public void TestSaveBoardWithNonRleFileTypeThrowsException()
        {
            Assert.Throws<NotImplementedException>(
                () => _gameOfLifeProcessor.SaveBoard(new Board(), "Glider.lif", GameOfLifeFileType.Life106, true),
                Resources.TestSaveBoardWithNonRleFileTypeThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that saving a board with a null pattern in the board throws an exception.
        /// </summary>
        [Test]
        public void TestSaveBoardWithNullPatternThrowsException()
        {
            Assert.Throws<ArgumentNullException>(
                () => _gameOfLifeProcessor.SaveBoard(new Board(), "Glider.rle", GameOfLifeFileType.Rle, true),
                Resources.TestSaveBoardWithNullPatternThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that saving a board with the trim pattern option trims the pattern and creates correct RLE content.
        /// </summary>
        [Test]
        public void TestSaveBoardWithTrimPatternOptionTrimsThePatternAndCreatesCorrectRleContent()
        {
            const string TrimmedRleGliderPattern = "#N Glider\r\nx = 3, y = 3, rule = B3/S23\r\nbob$2bo$3o!\r\n";
            bool[,] untrimmedGliderPattern = GetGliderPattern(50, 50);
            Board currentBoard = new Board();
            currentBoard.SetPattern(untrimmedGliderPattern);
            lock (_lockObject)
            {
                _fileContents = null;
                _gameOfLifeProcessor.SaveBoard(currentBoard, "Glider.rle", GameOfLifeFileType.Rle, true);
                Assert.AreEqual(TrimmedRleGliderPattern, _fileContents);
            }
        }

        /// <summary>
        /// Tests that saving a board without the trim pattern option does not trim the pattern and creates correct RLE content.
        /// </summary>
        [Test]
        public void TestSaveBoardWithoutTrimPatternOptionTrimsThePatternAndCreatesCorrectRleContent()
        {
            const string UntrimmedRleGliderPattern = "#N Glider\r\nx = 12, y = 12, rule = B3/S23\r\n12b$12b$12b$12b$5bo6b$6bo5b$4b3o5b$12b$12b$12b$12b$12b!\r\n";
            bool[,] untrimmedGliderPattern = GetGliderPattern(12, 12);
            Board currentBoard = new Board();
            currentBoard.SetPattern(untrimmedGliderPattern);
            lock (_lockObject)
            {
                _fileContents = null;
                _gameOfLifeProcessor.SaveBoard(currentBoard, "Glider.rle", GameOfLifeFileType.Rle, false);
                Assert.AreEqual(UntrimmedRleGliderPattern, _fileContents);
            }
        }

        /// <summary>
        /// Tests that saving a board with RLE file type with a large pattern will break lines after 70 characters.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        [Test]
        public void TestSaveBoardRleWithLargePatternWillBreakLinesAfterSeventyChars()
        {
            bool[,] bigPattern = new bool[300, 300];
            for (int x = 1; x < 300; x++)
            {
                for (int y = 1; y < 300; y++)
                {
                    bigPattern[x, y] = !bigPattern[x - 1, y - 1];
                }
            }

            Board currentBoard = new Board();
            currentBoard.SetPattern(bigPattern);
            string[] lines = { };
            lock (_lockObject)
            {
                _fileContents = null;
                _gameOfLifeProcessor.SaveBoard(currentBoard, "BigPattern.rle", GameOfLifeFileType.Rle, true);
                if (_fileContents != null)
                {
                    lines = _fileContents.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            int biggestLineLength = lines.Max(l => l.Length);
            Assert.IsTrue(
                biggestLineLength <= 70,
                string.Format(CultureInfo.CurrentCulture, Resources.TestSaveBoardRleWithLargePatternWillBreakLinesAfterSeventyCharsFailMessage, biggestLineLength));
        }

        /// <summary>
        /// Tests that loading a board with a file type different than RLE throws an exception.
        /// </summary>
        /// <remarks>
        /// This test should be removed once other file types get implemented.
        /// </remarks>
        [Test]
        public void TestLoadBoardWithNonRleFileTypeThrowsException()
        {
            Assert.Throws<NotImplementedException>(
                () => _gameOfLifeProcessor.LoadBoard("Glider.lif", GameOfLifeFileType.Life106, 3, 3),
                Resources.TestLoadBoardWithNonRleFileTypeThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board from a RLE file creates the correct pattern in the returned board.
        /// </summary>
        [Test]
        public void TestLoadBoardFromRleFileCreatesCorrectPatternWithinBoard()
        {
            Board currentBoard = _gameOfLifeProcessor.LoadBoard("Glider.rle", GameOfLifeFileType.Rle, 0, 0);
            bool[,] currentPattern = currentBoard.GetPattern();
            bool[,] expectedPattern = GetGliderPattern(3, 3);

            bool anyDifferent = (expectedPattern.GetLength(0) != currentPattern.GetLength(0)) || (expectedPattern.GetLength(1) != currentPattern.GetLength(1));
            if (!anyDifferent)
            {
                for (int x = 0; x < expectedPattern.GetLength(0); x++)
                {
                    for (int y = 0; y < expectedPattern.GetLength(1); y++)
                    {
                        anyDifferent = anyDifferent || (expectedPattern[x, y] != currentPattern[x, y]);
                    }
                }
            }

            Assert.IsFalse(anyDifferent, Resources.TestLoadBoardWithRleFileCreatesCorrectPatternInBoardFailMessage);
        }

        /// <summary>
        /// Tests that loading a board from a RLE file with some padding creates the correct sized pattern in the returned board.
        /// </summary>
        [Test]
        public void TestLoadBoardFromRleFileWithPaddingCreatesCorrectSizedPatternWithinBoard()
        {
            int width = _rnd.Next(100, 1025);
            int height = _rnd.Next(100, 1025);
            Board currentBoard = _gameOfLifeProcessor.LoadBoard("Glider.rle", GameOfLifeFileType.Rle, width, height);
            bool[,] currentPattern = currentBoard.GetPattern();

            string failedMessage = string.Format(
                CultureInfo.CurrentCulture,
                Resources.TestLoadBoardFromRleFileWithPaddingCreatesCorrectSizedPatternInBoardFailMessage,
                width,
                height,
                currentPattern.GetLength(0),
                currentPattern.GetLength(1));
            Assert.IsTrue((width == currentPattern.GetLength(0)) && (height == currentPattern.GetLength(1)), failedMessage);
        }

        /// <summary>
        /// Tests that loading a board from a RLE file with some padding creates the correct pattern in the returned board.
        /// </summary>
        [Test]
        public void TestLoadBoardFromRleFileWithPaddingCreatesCorrectPatternWithinBoard()
        {
            Board currentBoard = _gameOfLifeProcessor.LoadBoard("Glider.rle", GameOfLifeFileType.Rle, 30, 30);
            bool[,] currentPattern = currentBoard.GetPattern();
            bool[,] expectedPattern = GetGliderPattern(currentPattern.GetLength(0), currentPattern.GetLength(1));

            bool anyDifferent = false;
            for (int x = 0; x < expectedPattern.GetLength(0); x++)
            {
                for (int y = 0; y < expectedPattern.GetLength(1); y++)
                {
                    anyDifferent = anyDifferent || (expectedPattern[x, y] != currentPattern[x, y]);
                }
            }

            Assert.IsFalse(anyDifferent, Resources.TestLoadBoardFromRleFileWithPaddingCreatesCorrectPatternInBoardFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with an inexistent file throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithInexistentFileThrowsException()
        {
            Assert.Throws<FileNotFoundException>(
                () => _gameOfLifeProcessor.LoadBoard("NonExistent.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithInexistentFileThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a null file name throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithNullFileNameThrowsException()
        {
            Assert.Throws<FileNotFoundException>(
                () => _gameOfLifeProcessor.LoadBoard(null, GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithNullFileNameThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a file containing an incorrect pattern size in its header throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithFileContainingIncorrectPatternSizeInHeaderThrowsException()
        {
            Assert.Throws<IndexOutOfRangeException>(
                () => _gameOfLifeProcessor.LoadBoard("GliderIncorrectPatternSizeHeader.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithFileContainingIncorrectPatternSizeInHeaderThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a file describing a non B3/S23 pattern throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithFileDescribingNonB3S23PatternThrowsException()
        {
            Assert.Throws<FormatException>(
                () => _gameOfLifeProcessor.LoadBoard("NotB3S23Pattern.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithFileDescribingNonB3S23PatternThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a file without a header throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithFileWithoutHeaderThrowsException()
        {
            Assert.Throws<FormatException>(
                () => _gameOfLifeProcessor.LoadBoard("CorruptFile1.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithFileWithoutHeaderThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a file that is empty throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithEmptyFileThrowsException()
        {
            Assert.Throws<FormatException>(
                () => _gameOfLifeProcessor.LoadBoard("CorruptFile2.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithEmptyFileThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with a non-RLE file that does not describes a pattern throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithNonRleFileThrowsException()
        {
            Assert.Throws<FormatException>(
                () => _gameOfLifeProcessor.LoadBoard("CorruptFile3.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithNonRleFileThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Tests that loading a board with invalid pattern dimensions in its header throws an exception.
        /// </summary>
        [Test]
        public void TestLoadBoardWithInvalidPatternDimensionsInHeaderThrowsException()
        {
            Assert.Throws<FormatException>(
                () => _gameOfLifeProcessor.LoadBoard("CorruptFile4.rle", GameOfLifeFileType.Rle, 0, 0),
                Resources.TestLoadBoardWithInvalidPatternDimensionsInHeaderThrowsExceptionFailMessage);
        }

        /// <summary>
        /// Gets the glider pattern.
        /// </summary>
        /// <param name="width">The width of the pattern, if smaller than 3 it will be set to 3.</param>
        /// <param name="height">The height of the pattern, if smaller than 3 it will be set to 3.</param>
        /// <returns>
        /// The glider pattern, padded and centralised to match the given width and height.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static bool[,] GetGliderPattern(int width, int height)
        {
            if (width < 3)
            {
                width = 3;
            }

            if (height < 3)
            {
                height = 3;
            }

            bool[,] gliderPattern = new bool[width, height];
            int widthPadding = (int)Math.Floor((width - 3) / 2.0m);
            int heightPadding = (int)Math.Floor((height - 3) / 2.0m);

            gliderPattern[1 + widthPadding, 0 + heightPadding] = true;
            gliderPattern[2 + widthPadding, 1 + heightPadding] = true;
            gliderPattern[0 + widthPadding, 2 + heightPadding] = true;
            gliderPattern[1 + widthPadding, 2 + heightPadding] = true;
            gliderPattern[2 + widthPadding, 2 + heightPadding] = true;

            return gliderPattern;
        }

        /// <summary>
        /// Gets an empty pattern with a given size.
        /// </summary>
        /// <param name="width">The width of the pattern, if smaller than 0 it will be set to 0.</param>
        /// <param name="height">The height of the pattern, if smaller than 0 it will be set to 0.</param>
        /// <returns>An empty pattern with the given width and height.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private static bool[,] GetEmptyPattern(int width, int height)
        {
            if (width < 0)
            {
                width = 0;
            }

            if (height < 0)
            {
                height = 0;
            }

            bool[,] emptyPattern = new bool[width, height];
            return emptyPattern;
        }

        /// <summary>
        /// Gets a random pattern with the given width and height.
        /// </summary>
        /// <param name="width">The width of the pattern, if smaller than 0 it will be set to 0.</param>
        /// <param name="height">The height of the pattern, if smaller than 0 it will be set to 0.</param>
        /// <param name="padding">The padding around the pattern that will be guaranteed to only have dead cells.</param>
        /// <returns>
        /// A randomly filled pattern with the given width and height.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Justification = "A multidimensional array is the perfect representation of a game of life board and does not waste space.")]
        private bool[,] GetRandomPattern(int width, int height, int padding)
        {
            bool[,] result = GetEmptyPattern(width, height);

            for (int x = padding; x < (width - padding); x++)
            {
                for (int y = padding; y < (height - padding); y++)
                {
                    if (_rnd.Next(2) > 0)
                    {
                        result[x, y] = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Sets up the mock repository.
        /// </summary>
        private void SetUpMockRepository()
        {
            const string TrimmedRleGliderPattern = "#N Glider\r\nx = 3, y = 3, rule = B3/S23\r\nbob$2bo$3o!\r\n";
            const string IncorrectPatternSizeHeaderRleGliderPattern = "#N Glider\r\nx = 2, y = 1, rule = B3/S23\r\nbob$2bo$3o!\r\n";
            const string NotB3S23Pattern = "#N 2x2 glider\r\nx = 5, y = 4, rule = 125/36\r\n3o2b2$2bobo$bo!\r\n";
            const string CorruptFile1 = "#N Glider\r\nbob$2bo$3o!\r\n";
            const string CorruptFile2 = "";
            const string CorruptFile3 = "Not an RLE file.";
            const string CorruptFile4 = "#N Glider\r\nx = 3A, y = B, rule = B3/S23\r\nbob$2bo$3o!\r\n";

            _textFilesRepositoryMock = new Mock<ITextFilesRepository>();
            _textFilesRepositoryMock.Setup(t => t.SaveTextFile(It.IsAny<string>(), It.IsAny<string>()))
                .Callback((string fileContents, string fileName) => this.SetFileContents(fileContents));

            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "Glider.rle" == fileName))).Returns(TrimmedRleGliderPattern);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "GliderIncorrectPatternSizeHeader.rle" == fileName))).Returns(IncorrectPatternSizeHeaderRleGliderPattern);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "NotB3S23Pattern.rle" == fileName))).Returns(NotB3S23Pattern);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "CorruptFile1.rle" == fileName))).Returns(CorruptFile1);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "CorruptFile2.rle" == fileName))).Returns(CorruptFile2);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "CorruptFile3.rle" == fileName))).Returns(CorruptFile3);
            _textFilesRepositoryMock.Setup(t => t.LoadTextFile(It.Is<string>(fileName => "CorruptFile4.rle" == fileName))).Returns(CorruptFile4);
            _textFilesRepositoryMock.Setup(
                t =>
                t.LoadTextFile(
                    It.Is<string>(
                        fileName =>
                        (fileName != "Glider.rle") && (fileName != "GliderIncorrectPatternSizeHeader.rle") && (fileName != "NotB3S23Pattern.rle") && (fileName != "CorruptFile1.rle")
                        && (fileName != "CorruptFile2.rle") && (fileName != "CorruptFile3.rle") && (fileName != "CorruptFile4.rle")))).Throws<FileNotFoundException>();
        }

        /// <summary>
        /// Sets the file contents.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        private void SetFileContents(string fileContents)
        {
            _fileContents = fileContents;
        }
    }
}
