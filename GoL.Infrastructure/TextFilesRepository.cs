﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextFilesRepository.cs" company="Felipe Ceotto">
// Copyright © 2014 Felipe Ceotto.
// </copyright>
//
// This file is part of Game of Life (https://bitbucket.org/ceottaki/game-of-life/).
//
// Game of Life is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Game of Life is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Game of Life. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------------------------------------------------

namespace GoL.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using GoL.Logic;
    using GoL.Logic.Annotations;

    /// <summary>
    /// Represents a game of life repository.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Class is instantiated by dependency injection."), UsedImplicitly]
    internal class TextFilesRepository : ITextFilesRepository
    {
        /// <summary>
        /// Saves a text file with the given contents to the given file name.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <exception cref="System.ArgumentNullException">fileName; Value of fileName cannot be null or empty.</exception>
        public void SaveTextFile(string fileContents, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName", "Value of fileName cannot be null or empty.");
            }

            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            {
                streamWriter.Write(fileContents);
            }
        }

        /// <summary>
        /// Loads a text file with the given file name.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <returns>
        /// The contents of the loaded file.
        /// </returns>
        /// <exception cref="System.IO.FileNotFoundException">File was not found when trying to load a text file.</exception>
        /// <exception cref="System.IO.IOException">There was a problem reading the given file from the disk.</exception>
        public string LoadTextFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("File was not found when trying to load a text file.", fileName);
            }

            StringBuilder stringBuilder = new StringBuilder();
            using (StreamReader reader = new StreamReader(fileName))
            {
                char[] buffer = new char[2048];
                int charCount;
                while ((charCount = reader.Read(buffer, 0, buffer.Length)) > 0)
                {
                    stringBuilder.Append(buffer, 0, charCount);
                }
            }

            return stringBuilder.ToString();
        }
    }
}
